<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Route::post('register','API\RegisterController@register');

Route::middleware('auth:api')->group(function(){
    // Routes 
    Route::resource('categories','API\CategoryController');
    Route::resource('products','API\ProductController');
    Route::get('getCategories','API\ProductController@getDataCategories');
    Route::resource('stocks','API\StockController');
    Route::resource('transactions','API\TransactionController');
    Route::resource('refunds','API\RefundController');
    Route::get('/transaction/search/{id}','API\TransactionController@search');
});
