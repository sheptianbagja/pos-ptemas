<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();


Route::group(['middleware' => 'auth'], function () {
Route::get('/home', 'HomeController@index')->name('home');
Route::resource('categories','CategoryController');
Route::get('/categories/publish/{id}','CategoryController@publish')->name('categories.publish');
Route::get('/categories/draft/{id}','CategoryController@draft')->name('categories.draft');

Route::resource('product','ProductController');
Route::get('/product/publish/{id}','ProductController@publish')->name('product.publish');
Route::get('/product/draft/{id}','ProductController@draft')->name('product.draft');
Route::get('/product/search/{id}', 'ProductController@search');

Route::resource('stock','StockController');
Route::resource('transaction','TransactionController')->except(['edit','update','destroy']);
Route::get('/transaction/search/{id}', 'TransactionController@search');

Route::resource('refund','RefundController')->except(['edit','update','destroy','show']);
});
