@extends('layouts.master')

@section('content')
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <form id="signupForm" action="{{ route('refund.store') }}" method="POST">
                    @csrf
                <h4 class="form-header text-uppercase">
                  <i class="fa fa-bars"></i>
                   Buat Refund
                </h4>

                @if (session('status'))
                <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <div class="alert-icon contrast-alert">
                         <i class="icon-check"></i>
                        </div>
                        <div class="alert-message">
                          <span><strong>Success!</strong> {{ session('status') }}</span>
                        </div>
                      </div> 
                @endif
                

                <div class="form-group row">
                        <label for="input-11" class="col-sm-2 col-form-label">Pilih Transaksi</label>
                        <div class="col-sm-4">
                        <select name="transaction_id" class="form-control single-select select-barang" id="basic-select barang_id" >
                                    <option value="">Pilih Barang</option>
                                @foreach($transactions as $transaction)
                                    <option value="{{ $transaction->id }}">
                                        {{ $transaction->id }} - {{ ucwords($transaction->nama_customer) }}
                                    </option>
                                @endforeach
                        </select>
                        </div>
                        

                        <label for="input-10" class="col-sm-2 col-form-label">Tanggal Refund</label>
                        <div class="col-sm-4">
                          {{-- <input type="text" class="form-control" id="input-10" name="name"> --}}
                          <input type="text" id="default-datepicker" class="form-control" name="tanggal" value="{{ \Carbon\Carbon::now() }}" readonly>
                        </div>
                      </div>

                      <div class="form-group row">
                            <label for="input-10" class="col-sm-2 col-form-label">ID Transaksi</label>
                            <div class="col-sm-4">
                                    <input type="text" class="form-control" id="id" name="id"  readonly>
                            </div>
                            <label for="input-11" class="col-sm-2 col-form-label">Tanggal Transaksi</label>
                            <div class="col-sm-4">
                                {{-- <input type="text" class="form-control" id="tanggal" name="tanggal"  readonly> --}}
                                <input type="text" name="tanggal" class="form-control span2" value="" id="dp1"  readonly>
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="input-12" class="col-sm-2 col-form-label">Nama Customer</label>
                            <div class="col-sm-4">
                              <input type="text" class="form-control" id="nama_customer" name="nama_customer" readonly>
                            </div>
                            <label for="input-13" class="col-sm-2 col-form-label">No. Handphone</label>
                            <div class="col-sm-4">
                                    <input type="text" class="form-control" id="handphone" name="handphone"  readonly>
                            </div>
                          </div>

                          <div class="form-group row">
                                <label for="input-12" class="col-sm-2 col-form-label">Metode Pembayaran</label>
                                <div class="col-sm-4">
                                  <input type="text" class="form-control" id="metode_pembayaran" name="metode_pembayaran" readonly>
                                </div>
                                <label for="input-13" class="col-sm-2 col-form-label">Alamat</label>
                                <div class="col-sm-4">
                                        <input type="text" class="form-control" id="alamat" name="alamat" readonly>
                                        {{-- <input type="text" class="form-control" id="input-10" name="alamat" value="{{ $transaction->alamat }}" readonly> --}}
                                </div>
                              </div>

                              <div class="form-group row">
                                    <label for="input-12" class="col-sm-2 col-form-label">Total Harga Pembelian</label>
                                    <div class="col-sm-10">
                                      <input type="text" class="form-control" id="total" name="total" readonly>
                                    </div>
                                  </div>
      
                      <div class="form-group row">
                        <label for="input-17" class="col-sm-2 col-form-label">Alasan Refund</label>
                        <div class="col-sm-10">
                          <textarea class="form-control" name="note" rows="4" id="input-17"></textarea>
                        </div>
                      </div>

                      <div class="form-footer">
                          <button type="submit" class="btn btn-danger"><i class="fa fa-times"></i> CANCEL</button>
                          <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> SAVE</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div><!--End Row-->

@endsection

@section('top')

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link href="{{ asset('assets/css/datepicker.css') }}" rel="stylesheet" type="text/css">
@endsection

@section('bot')

  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
  <script src="{{ asset('assets/js/bootstrap-datepicker.js') }}"></script>


  <script>
      $(function(){
        // window.prettyPrint && prettyPrint();
        $('#dp1').datepicker({
          format: 'yyyy-mm-dd'
        });
      });
    </script>

<script>
        $(document).ready(function() {
            var i=1
    
            $('.select-barang').select2({
                placeholder: 'Pilih Transaksi'
            });
    
    
            $('.select-barang').on("select2:select", function(e) { 
                var id = $(this).val();
                $.get("/transaction/search/"+id, function(data, status){
                    $('#id').val(data.id);
                    $('#dp1').val(data.tanggal);
                    $('#nama_customer').val(data.nama_customer);
                    $('#alamat').val(data.alamat);
                    $('#handphone').val(data.handphone);
                    $('#metode_pembayaran').val(data.metode_pembayaran);
                    $('#total').val(data.total);
                });
                $('#qty').focus();
                // $('#diskon').focus();
            });
    
        });
    </script>

<script>
  // $('#default-datepicker').datepicker({
  //   todayHighlight: true
  // });
  // $('#autoclose-datepicker').datepicker({
  //   autoclose: true,
  //   todayHighlight: true
  // });

  // $('#inline-datepicker').datepicker({
  //    todayHighlight: true
  // });

  // $('#dateragne-picker .input-daterange').datepicker({
  //  });

</script>

<!--Multi Select Js-->
<script src="{{ asset('assets/plugins/jquery-multi-select/jquery.multi-select.js') }} "></script>
<script src="{{ asset('assets/plugins/jquery-multi-select/jquery.quicksearch.js') }} "></script>


<script>
    $(document).ready(function() {
        $('.single-select').select2();
  
        $('.multiple-select').select2();

    //multiselect start

        $('#my_multi_select1').multiSelect();
        $('#my_multi_select2').multiSelect({
            selectableOptgroup: true
        });

        $('#my_multi_select3').multiSelect({
            selectableHeader: "<input type='text' class='form-control search-input' autocomplete='off' placeholder='search...'>",
            selectionHeader: "<input type='text' class='form-control search-input' autocomplete='off' placeholder='search...'>",
            afterInit: function (ms) {
                var that = this,
                    $selectableSearch = that.$selectableUl.prev(),
                    $selectionSearch = that.$selectionUl.prev(),
                    selectableSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selectable:not(.ms-selected)',
                    selectionSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selection.ms-selected';

                that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                    .on('keydown', function (e) {
                        if (e.which === 40) {
                            that.$selectableUl.focus();
                            return false;
                        }
                    });

                that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                    .on('keydown', function (e) {
                        if (e.which == 40) {
                            that.$selectionUl.focus();
                            return false;
                        }
                    });
            },
            afterSelect: function () {
                this.qs1.cache();
                this.qs2.cache();
            },
            afterDeselect: function () {
                this.qs1.cache();
                this.qs2.cache();
            }
        });

     $('.custom-header').multiSelect({
          selectableHeader: "<div class='custom-header'>Selectable items</div>",
          selectionHeader: "<div class='custom-header'>Selection items</div>",
          selectableFooter: "<div class='custom-header'>Selectable footer</div>",
          selectionFooter: "<div class='custom-header'>Selection footer</div>"
        });



      });

</script>

<!--Form Validatin Script-->
<script src="{{ asset('assets/plugins/jquery-validation/js/jquery.validate.min.js') }}"></script>

<script>

        $().ready(function() {
    
        $("#personal-info").validate();
    
       // validate signup form on keyup and submit
        $("#signupForm").validate({
            rules: {
                note: {
                    required: true,
                    minlength: 5
                },
            },
            messages: {
                note: {
                    required: "Mohon, masukan alasan refund",
                    minlength: "Nama produk minimal harus 5 digit"
                },
            }
        });
    
    });
    
        </script>

@endsection

