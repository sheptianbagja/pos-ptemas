@extends('layouts.master')

@section('content')
<div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header"><i class="fa fa-table"></i>Data Refund</div>
            <div class="card-body">
                    <a href="{{ route('refund.create') }}"><button type="button" class="btn btn-primary waves-effect waves-light m-1">Buat Refund</button></a><br><br>
              
                    <form action="{{route('refund.index')}}">

                            <div class="form-group row">

                                <div class="col-sm-3">
                                        <input type="text" name="tanggal" class="form-control span2" value="" id="dp1" placeholder="Pilih tanggal" >
                                </div> 


                                    <div class="col-sm-3">
                                            <input 
                                            type="text" 
                                            class="form-control" 
                                            placeholder="Cari by id transaksi"
                                            name="transaction_id">
          
                                    </div>

                                    <div class="col-sm-3">
                                                <input 
                                                type="text" 
                                                class="form-control" 
                                                placeholder="Cari by nama customer"
                                                name="nama_customer">
              
                                        </div>


                                    <div class="col-sm-2">
                                            <input 
                                            type="submit" 
                                            value="Search / Filter" 
                                            class="btn btn-primary">
                                    </div>
                                  </div>
                    
                          </form>

                          @if (session('status'))
                          <div class="alert alert-success alert-dismissible" role="alert">
                                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                                  <div class="alert-icon contrast-alert">
                                   <i class="icon-check"></i>
                                  </div>
                                  <div class="alert-message">
                                    <span><strong>Success!</strong> {{ session('status') }}</span>
                                  </div>
                                </div> 
                          @endif
                          
              
                    <div class="table-responsive">
              <table  class="table table-hover">
                <thead class="thead-info shadow-info">
                    <tr>
                        <th>ID Transaction</th>
                        <th>Nama Customer</th>
                        <th>Tanggal Refund</th>
                        <th>Note</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($refunds as $refund)
                    <tr>
                        <td>{{ $refund->transaction_id }}</td>
                        <td>{{ ucwords($refund->nama_customer) }}</td>
                        <td>
                            <button type="button" class="btn btn-gradient-purpink waves-effect waves-light m-1"> {{ $refund->tanggal }}</button>
                         </td>
                        <td>{{ ucwords($refund->note) }}</td>
                    <tr>
                    @endforeach
                </tbody>
                </table>
                <br>
                {{ $refunds->links() }}
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->
@endsection

@section('top')
    <!--Data Tables -->
    <link href="{{ asset('assets/plugins/bootstrap-datatable/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/plugins/bootstrap-datatable/css/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/datepicker.css') }}" rel="stylesheet" type="text/css">
@endsection

@section('bot')
<!--Data Tables js-->
    <script src="{{ asset('assets/plugins/bootstrap-datatable/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-datatable/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-datatable/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-datatable/js/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-datatable/js/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-datatable/js/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-datatable/js/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-datatable/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-datatable/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-datatable/js/buttons.colVis.min.js') }}"></script>
    <script src="{{ asset('assets/js/bootstrap-datepicker.js') }}"></script>

   <script>
        $(function(){
			// window.prettyPrint && prettyPrint();
			$('#dp1').datepicker({
				format: 'yyyy-mm-dd'
			});
	});
    </script>

    <script>
            $(document).ready(function() {
             //Default data table
              $('#default-datatable').DataTable();
       
       
              var table = $('#example').DataTable( {
               lengthChange: false,
               buttons: [ 'copy', 'excel', 'pdf', 'print', 'colvis' ]
             } );
        
            table.buttons().container()
               .appendTo( '#example_wrapper .col-md-6:eq(0)' );
             
             } );
       
           </script>
@endsection

