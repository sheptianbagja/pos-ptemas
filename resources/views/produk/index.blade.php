@extends('layouts.master') 
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header"><i class="fa fa-table"></i>Data Produk</div>
            <div class="card-body">
                <a href="{{ route('product.create') }}"><button type="button" class="btn btn-primary waves-effect waves-light m-1">Tambah Data</button></a><br><br>               
                 
                <form action="{{route('product.index')}}">

                    <div class="form-group row">
                        <div class="col-sm-2">
                            <input type="text" class="form-control" placeholder="Nama Produk" value="{{Request::get('name')}}"
                                name="name">

                        </div>

                        <div class="col-sm-2">
                            <input type="text" class="form-control" placeholder="Min Harga" value="{{Request::get('min_price')}}"
                                name="min_price">

                        </div>

                        <div class="col-sm-2">
                            <input type="text" class="form-control" placeholder="Max Harga" value="{{Request::get('max_price')}}"
                                name="max_price">

                        </div>

                        <div class="col-sm-2">
                            <select class="form-control" id="basic-select" name="status">
                                <option value="">Pilih Status</option>
                                <option name="publish">Publish</option>
                                <option name="draft">Draft</option>
                            </select>
                        </div>

                        <div class="col-sm-2">
                            <select name="category_id" class="form-control" id="basic-select">
                                    <option value="">Pilih Kategori</option>
                                @foreach($categories as $category)
                                    <option value="{{ $category->id }}">{{ ucwords($category->name) }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="col-sm-2">
                            <input type="submit" value="Search / Filter" class="btn btn-primary">
                        </div>
                    </div>

                </form> 
                
                @if (session('status'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <div class="alert-icon contrast-alert">
                        <i class="icon-check"></i>
                    </div>
                    <div class="alert-message">
                        <span><strong>Success!</strong> {{ session('status') }}</span>
                    </div>
                </div>
                @endif


                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead class="thead-info shadow-info">
                            <tr>
                                <th>Id</th>
                                <th>Stok</th>
                                {{-- <th>Deskripsi</th> --}}
                                <th>Nama</th>
                                <th>Harga</th>
                                <th>Gambar</th>
                                <th>Warna</th>
                                <th>Kategori</th>
                                <th>Status</th>
                                {{--
                                <th>Created At</th>
                                <th>Updated At</th> --}}
                                <th width="20px"></th>
                                <th width="70px"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $no =1;
                            @endphp
                            @foreach ($products as $product)
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td>{{ $product->stok }}</td>
                                <td>{{ ucwords($product->name) }}</td>
                                {{-- <td>{{ $product->description }}</td> --}}
                                <td>Rp.{{  number_format($product->price,0) }}</td>
                                <td><img class="rounded-square" width="50" height="50" src="{{ url($product->image) }}" alt="gambar produk"></td>
                                <td>{{ $product->color }}</td>
                                <td>{{ $product->category->name }}</td>
                                <td>
                                    @if ($product->status == 'publish')
                                    <button type="button" class="btn btn-inverse-primary waves-effect btn-sm">{{ $product->status }}</button>                                    @elseif ($product->status == 'draft')
                                    <button type="button" class="btn btn-inverse-danger waves-effect btn-sm">{{ $product->status }}</button>                                    @endif
                                </td>
                                {{--
                                <td>{{ $product->created_at }}</td>
                                <td>{{ $product->updated_at }}</td> --}}
                                <td>
                                    <a href="{{ url('/product/publish/'.$product->id) }}" class="btn btn-gradient-bloody btn-sm">Publish</a>
                                    <a href="{{ url('/product/draft/'.$product->id) }}" class="btn btn-gradient-scooter btn-sm">Draft</a>

                                    <a href="{{ route('product.edit', ['id' => $product->id]) }}" class="btn btn-gradient-purpink btn-sm">Ubah</a>
                                </td>
                                <td>
                                    <form class="d-inline" action="{{route('product.destroy', ['id' => $product->id])}}" method="POST" onsubmit="return confirm('Anda yakin akan menghapus data ini dan memindahkan ke tong sampah ?')">

                                        @csrf

                                        <input type="hidden" value="DELETE" name="_method">

                                        <input type="submit" class="btn btn-gradient-ibiza btn-sm" value="Hapus">

                                    </form>
                                </td>
                                <tr>
                                    @endforeach
                        </tbody>
                        {{--
                        <tfoot class="thead-danger shadow-danger">
                            <th>ID</th>
                            <th>Name</th>
                            <th>Slug</th>
                            <th>Created At</th>
                            <th>Updated At</th>
                        </tfoot> --}}
                    </table>
                    <br> {{ $products->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Row-->
@endsection
 
@section('top')
<!--Data Tables -->
<link href="{{ asset('assets/plugins/bootstrap-datatable/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('assets/plugins/bootstrap-datatable/css/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css">
@endsection
 
@section('bot')
<!--Data Tables js-->
<script src="{{ asset('assets/plugins/bootstrap-datatable/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap-datatable/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap-datatable/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap-datatable/js/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap-datatable/js/jszip.min.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap-datatable/js/pdfmake.min.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap-datatable/js/vfs_fonts.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap-datatable/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap-datatable/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap-datatable/js/buttons.colVis.min.js') }}"></script>

<script>
    $(document).ready(function() {
             //Default data table
              $('#default-datatable').DataTable();
       
       
              var table = $('#example').DataTable( {
               lengthChange: false,
               buttons: [ 'copy', 'excel', 'pdf', 'print', 'colvis' ]
             } );
        
            table.buttons().container()
               .appendTo( '#example_wrapper .col-md-6:eq(0)' );
             
             } );
</script>
@endsection