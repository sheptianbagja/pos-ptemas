<h4 class="form-header text-uppercase">
    <i class="fa fa-cube"></i> Produk
</h4>
<div class="form-group row">
        <div class="col-sm-3">
                <select name="barang_id" class="form-control single-select select-barang" id="basic-select barang_id" placeholder="Pilih Barang">
                        @foreach($products as $product)
                            <option value="{{ $product->id }}">{{ ucwords($product->name) }}</option>
                        @endforeach
                </select>
            </div>
    <div class="col-sm-2">
        {{-- <input type="text" class="form-control" id="harga" name="_harga" placeholder="Harga">
        <input type="text" class="form-control" id="id_product" name="_id_product" placeholder="Id product" readonly>
        <input type="text" class="form-control" id="nama_product" name="_nama_product"  placeholder="Nama product" readonly>
        <input type="text" class="form-control" id="sub_total" name="_sub_total" placeholder="Sub total" readonly> --}}

        <input type="text" class="form-control" id="harga" name="_harga" placeholder="Harga" readonly>
        <input type="hidden" class="form-control" id="kode" name="_kode" placeholder="Id product" readonly>
        <input type="hidden" class="form-control" id="nama" name="_nama"  placeholder="Nama product" readonly>
        <input type="hidden" class="form-control" id="subtotal" name="_subtotal" placeholder="Sub total" readonly>
        <input type="hidden" class="form-control" id="total_diskon" name="_total_diskon" placeholder="Total Diskon" readonly>
        <input type="hidden" class="form-control" id="total_hargaAwal" name="_total_hargaAwal" placeholder="Total Harga Awal" readonly>
        {{-- <input type="hidden" class="form-control" id="subtotal" name="_subtotal" placeholder="Sub total" readonly> --}}
    </div>

    <div class="col-sm-2">
        <input type="text" class="form-control" id="qty" name="_qty" placeholder="Jumlah">
    </div>
    
    <div class="col-sm-2">
        <input type="text" class="form-control" id="diskon" name="_diskon" placeholder="Diskon">
    </div>

    <div class="col-sm-2">
        <button class="btn btn-info" id="btn-tambah">Tambah</button>
    </div>
</div>

<h4 class="form-header text-uppercase">
        <i class="fa fa-cube"></i> Daftar Produk 
    </h4>

<div class="form-group col-md-12">
        <div class="row" style="border-bottom: 1px solid #eeeeee;margin-bottom: 15px;padding-bottom: 5px;">
            <div class="col-md-1">No</div>
            <div class="col-md-1">Kode</div>
            <div class="col-md-2">Nama</div>
            <div class="col-md-2">Harga</div>
            <div class="col-md-1">Qty</div>
            <div class="col-md-1">Diskon</div>
            <div class="col-md-2">Total Diskon</div>
            {{-- <div class="col-md-2">Total Diskon</div> --}}
            <div class="col-md-2">Subtotal</div>
        </div>
        <div id="daftar-penjualan">
    
        </div>
    </div>

