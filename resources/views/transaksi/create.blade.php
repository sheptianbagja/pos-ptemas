@extends('layouts.master')

@section('content')
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <form id="signupForm" action="{{ route('transaction.store') }}" method="POST" >
                    @csrf
                <h4 class="form-header text-uppercase">
                  <i class="fa fa-bars"></i>
                   Buat Transaksi
                </h4>

                @if (session('status'))
                <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <div class="alert-icon contrast-alert">
                         <i class="icon-check"></i>
                        </div>
                        <div class="alert-message">
                          <span><strong>Success!</strong> {{ session('status') }}</span>
                        </div>
                      </div> 
                @endif
                

                <div class="form-group row">
                        <label for="input-10" class="col-sm-2 col-form-label">Tanggal</label>
                        <div class="col-sm-4">
                          {{-- <input type="text" class="form-control" id="input-10" name="name"> --}}
                          <input type="text" id="default-datepicker" class="form-control" name="tanggal" value="{{ \Carbon\Carbon::now() }}">
                        </div>
                        <label for="input-11" class="col-sm-2 col-form-label">Nama Pelanggan</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" id="input-10" name="nama_customer">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="input-12" class="col-sm-2 col-form-label">Handphone</label>
                        <div class="col-sm-4">
                          <input type="text" class="form-control" id="input-12" name="handphone">
                        </div>
                        <label for="input-13" class="col-sm-2 col-form-label">Metode Pembayaran</label>
                        <div class="col-sm-4">
                                <select class="form-control" id="basic-select" name="metode_pembayaran" >
                                    <option value="cash">Cash</option>
                                    <option value="transfer">Transfer</option>
                                </select>
                        </div>
                      </div>
                      {{-- <h4 class="form-header text-uppercase">
                      <i class="fa fa-envelope-o"></i>
                        Contact Info & Bio
                      </h4> --}}
      
                      <div class="form-group row">
                        <label for="input-17" class="col-sm-2 col-form-label">Alamat</label>
                        <div class="col-sm-10">
                          <textarea class="form-control" name="alamat" rows="4" id="input-17"></textarea>
                        </div>
                      </div>

                      <div class="form-group row">

                          <label for="input-14" class="col-sm-2 col-form-label">Bayar</label>
                          <div class="col-sm-4">
                            <input type="text" class="form-control" id="total_bayar" name="total_bayar">
                          </div>

                            <label for="input-14" class="col-sm-2 col-form-label">Total</label>
                            <div class="col-sm-4">
                              <input type="text" class="form-control" id="total" name="total" readonly>
                            </div>
                          </div>

                          <div class="form-group row">
    
                                <label for="input-14" class="col-sm-2 col-form-label">Kembalian</label>
                                <div class="col-sm-10">
                                  <input type="text" class="form-control" id="kembalian" name="kembalian" readonly>
                                </div>
                              </div>

                      @include('transaksi.field')

                      <div class="form-footer">
                          <button type="submit" class="btn btn-danger"><i class="fa fa-times"></i> CANCEL</button>
                          <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> SAVE</button>
                      </div>

                      
                    </form>
                  </div>
                </div>
              </div>
            </div><!--End Row-->

@endsection

@section('top')

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

    {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.min.css"> --}}

    <!--Select Plugins-->
  {{-- <link href="{{ asset('assets/plugins/select2/css/select2.min.css') }} " rel="stylesheet" /> --}}
  <!--inputtags-->
  {{-- <link href="{{ asset('assets/plugins/inputtags/css/bootstrap-tagsinput.css') }} " rel="stylesheet" /> --}}
  <!--multi select-->
  {{-- <link href="{{ asset('assets/plugins/jquery-multi-select/multi-select.css') }} " rel="stylesheet" type="text/css"> --}}
  <!--Bootstrap Datepicker-->
  {{-- <link href="{{ asset('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }} " rel="stylesheet" type="text/css">
  <!--Touchspin-->
  <link href="{{ asset('assets/plugins/bootstrap-touchspin/css/jquery.bootstrap-touchspin.css') }} " rel="stylesheet" type="text/css"> --}}
@endsection

@section('bot')

<!--Bootstrap Touchspin Js-->
{{-- <script src="{{ asset('assets/plugins/bootstrap-touchspin/js/jquery.bootstrap-touchspin.js') }} "></script> --}}
{{-- <script src="{{ asset('assets/plugins/bootstrap-touchspin/js/bootstrap-touchspin-script.js') }} "></script> --}}

{{-- <!--Select Plugins Js-->
<script src="{{ asset('assets/plugins/select2/js/select2.min.js') }} "></script> --}}
<!--Inputtags Js-->
{{-- <script src="{{ asset('assets/plugins/inputtags/js/bootstrap-tagsinput.js') }} "></script> --}}

<!--Bootstrap Datepicker Js-->
{{-- <script src="{{ asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }} "></script> --}}

  <!-- jQuery 3.1.1 -->
  {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> --}}
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

  <!-- AdminLTE App -->
  {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.4.2/js/adminlte.min.js"></script> --}}

  <script src="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

<script>
        $(document).ready(function() {
            var i=1

            // function getNum(val) {
            //   val = +val || 0
            //   return val;
            // }
    
            $('.select-barang').select2({
                // theme: "bootstrap",
                placeholder: 'Pilih Barang'
            });
    
    
            $('.select-barang').on("select2:select", function(e) { 
                var id = $(this).val();
                $.get("/product/search/"+id, function(data, status){
                    $('#kode').val(data.id);
                    $('#nama').val(data.name);
                    $('#harga').val(data.price);
                });
                $('#qty').focus();
                // $('#diskon').focus();
            });
    
            
            $('#btn-tambah').on('click',function(e){

              // parseInt('') === 0;

              // var diskon = parseInt($('#diskon').val())
              //   if (isNan(diskon)) diskon == 0;

                // var total_harga_diskon = val(((parseInt($('#harga').val()) * parseInt($('#diskon').val())) / 100)  * parseInt($('#qty').val()));
                // var total_harga_awal = val(parseInt($('#harga').val())* parseInt($('#diskon').val()));
                // var hasil = total_harga_awal - total_harga_diskon;
                // $('#subtotal').val(hasil);

                // var total_harga_diskon = (parseInt($('#harga').val()) * (parseInt($('#diskon').val()) / 100)) * parseInt($('#qty').val());
                // var total_harga_awal = parseInt($('harga').val()) * parseInt($('qty').val());
                // var hasil = total_harga_awal - total_harga_diskon;

                // $('subtotal').val(hasil);

                $('#total_diskon').val(((parseInt($('#harga').val()) * parseInt($('#diskon').val())) / 100)  * parseInt($('#qty').val()));
                // $('#subtotal').val(parseInt($('#harga').val()) * parseInt($('#qty').val()));
                $('#total_hargaAwal').val(parseInt($('#harga').val()) * parseInt($('#qty').val()));
                // $('#subtotal').val((parseInt($('#harga').val()) * parseInt($('#qty').val()))) - parseInt($('total_diskon').val());
                $('#subtotal').val(parseInt($('#total_hargaAwal').val()) - parseInt($('#total_diskon').val()));

                

                // $('#subtotal').val(((parseInt($('#harga').val()) * diskon) / 100)  * parseInt($('#qty').val()));

              //  $('#diskon').val(parseInt($('#diskon')));
                // $('#subtotal').val(((parseInt($('#harga').val()) * parseInt($('#diskon').val())) / 100));
                // $('#diskon').val(parseInt($('#harga').val())*parseInt($('#diskon').val()));
    
                // if (isNaN(test)) test = 0;

                
                
                $("#daftar-penjualan").append('<div class="row">'+
                    '<div class="col-md-1">'+i+'</div>'+
                    '<div class="col-md-1"><input type="text" readonly class="form-control" name="kode[]" value="'+$('#kode').val()+'"></div>'+
                    '<div class="col-md-2"><input type="text" readonly class="form-control" name="nama[]" value="'+$('#nama').val()+'"></div>'+
                    '<div class="col-md-2 "><input type="text" readonly class="text-right form-control" name="harga[]" value="'+$('#harga').val()+'"></div>'+
                    '<div class="col-md-1 "><input type="text" readonly class="text-right form-control" name="qty[]" value="'+$('#qty').val()+'"></div>'+
                    // '<div class="col-md-2 "><input type="text" readonly class="text-right form-control" name="diskon[]" value="'+diskon+'"></div>'+

                    '<div class="col-md-1 "><input type="text" readonly class="text-right form-control" name="diskon[]" value="'+$('#diskon').val()+'"></div>'+
                    '<div class="col-md-2 "><input type="text" readonly class="text-right form-control total_diskon" name="total_diskon[]" value="'+$('#total_diskon').val()+'"></div>'+
                    // '<div class="col-md-2 "><input type="hidden" readonly class="text-right form-control total_hargaAwal" name="total_hargaAwal[]" value="'+$('#total_hargaAwal').val()+'"></div>'+
                    '<div class="col-md-2 "><input type="text" readonly class="text-right form-control subtotal" name="subtotal[]" value="'+$('#subtotal').val()+'"></div>'+
                '</div>');
                i++;
                $('#kode').val('');
                $('#nama').val('');
                $('#harga').val('');
                $('#qty').val('');
                $('#diskon').val();
                $('#total_diskon').val();
                $('#total_hargaAwal').val();
                // $('#diskon').val(parseInt($('#total_bayar').val());
                // $('#diskon').val((+$('#diskon').val() || 0));
                $('#subtotal').val();
                $('.select-barang').val(null).trigger('change');
    
                var total = 0;
                $(".subtotal").each(function() {
                    total += parseInt($(this).val());
                });
                $('#total').val(total);

                
                $('#kembalian').val(parseInt($('#total_bayar').val()) - parseInt($('#total').val()));
                
                // var total_bayar = val(parseInt($('#total_bayar').val()) - parseInt($('#total').val()));
                // $('#total_bayar').val(total_bayar);

                e.preventDefault();
            })

            $('#total_bayar').on('keyup',function(e){
              $('#kembalian').val(parseInt($('#total_bayar').val()) - parseInt($('#total').val()));
              // var total_bayar = val(parseInt($('#total_bayar').val()) - parseInt($('#total').val()));
              
              e.preventDefault();
            })
        });
    </script>

<script>
  // $('#default-datepicker').datepicker({
  //   todayHighlight: true
  // });
  // $('#autoclose-datepicker').datepicker({
  //   autoclose: true,
  //   todayHighlight: true
  // });

  // $('#inline-datepicker').datepicker({
  //    todayHighlight: true
  // });

  // $('#dateragne-picker .input-daterange').datepicker({
  //  });

</script>

<!--Multi Select Js-->
<script src="{{ asset('assets/plugins/jquery-multi-select/jquery.multi-select.js') }} "></script>
<script src="{{ asset('assets/plugins/jquery-multi-select/jquery.quicksearch.js') }} "></script>


<script>
    $(document).ready(function() {
        $('.single-select').select2();
  
        $('.multiple-select').select2();

    //multiselect start

        $('#my_multi_select1').multiSelect();
        $('#my_multi_select2').multiSelect({
            selectableOptgroup: true
        });

        $('#my_multi_select3').multiSelect({
            selectableHeader: "<input type='text' class='form-control search-input' autocomplete='off' placeholder='search...'>",
            selectionHeader: "<input type='text' class='form-control search-input' autocomplete='off' placeholder='search...'>",
            afterInit: function (ms) {
                var that = this,
                    $selectableSearch = that.$selectableUl.prev(),
                    $selectionSearch = that.$selectionUl.prev(),
                    selectableSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selectable:not(.ms-selected)',
                    selectionSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selection.ms-selected';

                that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                    .on('keydown', function (e) {
                        if (e.which === 40) {
                            that.$selectableUl.focus();
                            return false;
                        }
                    });

                that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                    .on('keydown', function (e) {
                        if (e.which == 40) {
                            that.$selectionUl.focus();
                            return false;
                        }
                    });
            },
            afterSelect: function () {
                this.qs1.cache();
                this.qs2.cache();
            },
            afterDeselect: function () {
                this.qs1.cache();
                this.qs2.cache();
            }
        });

     $('.custom-header').multiSelect({
          selectableHeader: "<div class='custom-header'>Selectable items</div>",
          selectionHeader: "<div class='custom-header'>Selection items</div>",
          selectableFooter: "<div class='custom-header'>Selectable footer</div>",
          selectionFooter: "<div class='custom-header'>Selection footer</div>"
        });



      });

</script>

<!--Form Validatin Script-->
<script src="{{ asset('assets/plugins/jquery-validation/js/jquery.validate.min.js') }}"></script>

<script>

$(document).ready(function() {
    
        $("#personal-info").validate();
    
       // validate signup form on keyup and submit
        $("#signupForm").validate({
            rules: {
                category_id: "required",
                total_bayar: "required",
                // image: "required",
                // status: "required",
                metode_pembayaran: "required",
                nama_customer: {
                    required: true,
                    minlength: 3
                },
                handphone: {
                    required: true,
                    minlength: 12,
                },
                alamat: {
                    required: true,
                    minlength: 5,
                },
            },
            messages: {
                category_id: "Mohon, masukan kategori produk",
                total_bayar: "Mohon, masukan pembayaran anda",
                // image: "Mohon, upload gambar produk",
                status: "Mohon masukan status produk",
                name: {
                    required: "Mohon, masukan nama produk",
                    minlength: "Nama produk minimal harus 3 digit"
                },
                price: {
                    required: "Mohon, masukan harga produk",
                    minlength: "Nama produk minimal harus 3 digit"
                },
                color: {
                    required: "Mohon, masukan warna produk",
                    minlength: "Nama produk minimal harus 3 digit"
                },
                description: {
                    required: "Mohon, masukan deskirpsi produk",
                    minlength: "Nama produk minimal harus 5 digit"
                },
                metode_pembayaran: "Mohon pilih metode pembayaran",
                nama_customer: {
                    required: "Mohon, masukan nama customer",
                    minlength: "Nama customer minimal harus 3 digit"
                },
                handphone: {
                    required: "Mohon, masukan no handphone",
                    minlength: "Handphone minimal harus 13 digit"
                },
                alamat: {
                    required: "Mohon, masukan warna produk",
                    minlength: "Alamat minimal harus 5 digit"
                },
            }
        });
    
    });
    
        </script>

@endsection

