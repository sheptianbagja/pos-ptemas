@extends('layouts.master') 
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header"><i class="fa fa-table"></i>Data Transaksi</div>
            <div class="card-body">
                <a href="{{ route('transaction.create') }}"><button type="button" class="btn btn-primary waves-effect waves-light m-1">Tambah Data</button></a><br><br>               
                 
                {{-- SEARCH / FILTERING --}}
                <form action="{{ route('transaction.index')}}">

                        <div class="form-group row">
                                <div class="col-sm-3">
                                        <input type="text" name="tanggal" class="form-control span2" value="" id="dp1" placeholder="Pilih tanggal" >
                                </div> 

                                <div class="col-sm-3">
                                        <input 
                                        type="text" 
                                        class="form-control" 
                                        placeholder="cari customer"
                                        value="{{Request::get('nama_customer')}}"
                                        name="nama_customer">
                                </div> 

                                <div class="col-sm-3">
                                        <select class="form-control" id="basic-select" name="metode_pembayaran">
                                                <option value="">Pilih metode bayar</option>
                                                <option value="cash">Cash</option>
                                                <option value="transfer">Transfer</option>
                                              </select>
                                </div>
                                <div class="col-sm-2">
                                        <input 
                                        type="submit" 
                                        value="Search / Filter" 
                                        class="btn btn-primary">
                                </div>
                              </div>
                
                      </form> 
                
                @if (session('status'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <div class="alert-icon contrast-alert">
                        <i class="icon-check"></i>
                    </div>
                    <div class="alert-message">
                        <span><strong>Success!</strong> {{ session('status') }}</span>
                    </div>
                </div>
                @endif


                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead class="thead-info shadow-info">
                            <tr>
                                <th>ID</th>
                                <th>Tanggal</th>
                                <th>Customer</th>
                                <th>Metode Pembayaran</th>
                                <th>Total</th>
                                <th>Status</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $no = 1;
                            @endphp
                            @foreach ($transactions as $transaction)
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    <td>{{ $transaction->tanggal }}</td>
                                    <td>{{ ucwords($transaction->nama_customer) }}</td>
                                    <td>@if ($transaction->metode_pembayaran == 'cash')
                                            <button type="button" class="btn btn-gradient-ibiza waves-effect ">{{ $transaction->metode_pembayaran }}</button>                                    
                                        @elseif ($transaction->metode_pembayaran == 'transfer')
                                            <button type="button" class="btn btn-gradient-steelgray waves-effect ">{{ $transaction->metode_pembayaran }}</button>                                   
                                         @endif
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-dark waves-effect ">
                                            Rp. {{  number_format($transaction->total) }}
                                        </button>
                                    </td>
                                    <td>
                                        @if ($transaction->status == 'done')
                                            <button type="button" class="btn btn-success waves-effect ">{{ $transaction->status }}</button>                                    
                                        @elseif ($transaction->status == 'refund')
                                            <button type="button" class="btn btn-danger waves-effect ">{{ $transaction->status }}</button>                                   
                                         @endif
                                    </td>
                                    {{--
                                    <td>{{ $transaction->created_at }}</td>
                                    <td>{{ $transaction->updated_at }}</td> --}}
                                    <td>
                                        <a href="{{ route('transaction.show', ['id' => $transaction->id]) }}" class="btn btn-gradient-purpink">Show</a>
                                    </td>
                                <tr>
                            @endforeach
                        </tbody>
                        {{--
                        <tfoot class="thead-danger shadow-danger">
                            <th>ID</th>
                            <th>Name</th>
                            <th>Slug</th>
                            <th>Created At</th>
                            <th>Updated At</th>
                        </tfoot> --}}
                    </table>
                    <br> {{ $transactions->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Row-->
@endsection
 
@section('top')
<!--Data Tables -->
<link href="{{ asset('assets/plugins/bootstrap-datatable/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('assets/plugins/bootstrap-datatable/css/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('assets/css/datepicker.css') }}" rel="stylesheet" type="text/css">
@endsection
 
@section('bot')
<!--Data Tables js-->
<script src="{{ asset('assets/plugins/bootstrap-datatable/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap-datatable/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap-datatable/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap-datatable/js/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap-datatable/js/jszip.min.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap-datatable/js/pdfmake.min.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap-datatable/js/vfs_fonts.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap-datatable/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap-datatable/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap-datatable/js/buttons.colVis.min.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap-datepicker.js') }}"></script>

<script>
		$(function(){
			// window.prettyPrint && prettyPrint();
			$('#dp1').datepicker({
				format: 'yyyy-mm-dd'
			});
		});
	</script>

<script>
    $(document).ready(function() {
             //Default data table
              $('#default-datatable').DataTable();
       
       
              var table = $('#example').DataTable( {
               lengthChange: false,
               buttons: [ 'copy', 'excel', 'pdf', 'print', 'colvis' ]
             } );
        
            table.buttons().container()
               .appendTo( '#example_wrapper .col-md-6:eq(0)' );
             
             } );
</script>
@endsection