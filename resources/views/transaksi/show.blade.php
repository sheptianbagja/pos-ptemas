@extends('layouts.master')

@section('content')
<div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header"><i class="fa fa-table"></i>Data Detail Transaksi</div>
            <div class="card-body">

                          @if (session('status'))
                          <div class="alert alert-success alert-dismissible" role="alert">
                                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                                  <div class="alert-icon contrast-alert">
                                   <i class="icon-check"></i>
                                  </div>
                                  <div class="alert-message">
                                    <span><strong>Success!</strong> {{ session('status') }}</span>
                                  </div>
                                </div> 
                          @endif
                          
                          <div class="form-group row">
                                <label for="input-10" class="col-sm-2 col-form-label">ID Transaksi</label>
                                <div class="col-sm-4">
                                  {{-- <input type="text" class="form-control" id="input-10" name="name"> --}}
                                  <input type="text" id="default-datepicker" class="form-control" name="tanggal" value="{{ $transaction->id }}" readonly>
                                </div>
                                <label for="input-11" class="col-sm-2 col-form-label">Tanggal</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" id="input-10" name="tanggal" value="{{ $transaction->tanggal }}" readonly>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="input-12" class="col-sm-2 col-form-label">Nama Customer</label>
                                <div class="col-sm-4">
                                  <input type="text" class="form-control" id="input-12" name="nama_customer" value="{{ ucwords($transaction->nama_customer) }}" readonly>
                                </div>
                                <label for="input-13" class="col-sm-2 col-form-label">No. Handphone</label>
                                <div class="col-sm-4">
                                        <input type="text" class="form-control" id="input-10" name="handphone" value="{{ $transaction->handphone }}" readonly>
                                </div>
                              </div>

                              <div class="form-group row">
                                    <label for="input-12" class="col-sm-2 col-form-label">Metode Pembayaran</label>
                                    <div class="col-sm-4">
                                      <input type="text" class="form-control" id="input-12" name="metode_pembayaran" value="{{ ucwords($transaction->metode_pembayaran) }}" readonly>
                                    </div>
                                    <label for="input-13" class="col-sm-2 col-form-label">Alamat</label>
                                    <div class="col-sm-4">
                                            <textarea class="form-control" id="input-10" name="alamat" disabled>{{ $transaction->alamat }}</textarea>
                                            {{-- <input type="text" class="form-control" id="input-10" name="alamat" value="{{ $transaction->alamat }}" readonly> --}}
                                    </div>
                                  </div>
              
                    <div class="table-responsive">
              <table  class="table table-hover">
                <thead class="thead-info shadow-info">
                    @php
                     $no = 1   
                    @endphp
                    <tr>
                        <th>No</th>
                        <th>ID Produk</th>
                        <th>Nama</th>
                        <th>Gambar Produk</th>
                        <th>Jumlah</th>
                        <th>Diskon</th>
                        <th>Subtotal</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($transaction->detail_transactions as $row=>$item)
                    <tr>
                        <td>{{ $no++ }}</td>
                        <td>{{ $item->product($item->product_id)->id }}</td>
                        <td>{{ ucwords($item->product($item->product_id)->name) }}</td>
                        <td><img class="rounded-square" width="50" height="50" src="{{ url($item->product($item->product_id)->image) }}" alt="gambar produk"></td>
                        <td>{{ $item->qty }}</td>
                        <td>{{ $item->diskon }} %</td>
                        <td>
                                <button type="button" class="btn btn-dark waves-effect ">
                                        Rp. {{  number_format($item->subtotal) }}
                                </button>
                        </td>
                    <tr>
                    @endforeach
                </tbody>
                <tr>
                    <td colspan="6">
                            <button type="button" class="btn btn-danger btn-block waves-effect waves-light mb-1">Total</button>
                    </td>
                    <td>
                            <button type="button" class="btn btn-dark waves-effect ">
                                    Rp. {{  number_format($transaction->total) }}
                            </button>
                    </td>
                </tr>
                </table>
                <br>
                {{-- {{ $transcation->links() }} --}}
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->
@endsection

@section('top')
    <!--Data Tables -->
    <link href="{{ asset('assets/plugins/bootstrap-datatable/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/plugins/bootstrap-datatable/css/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css">
@endsection

@section('bot')
<!--Data Tables js-->
    <script src="{{ asset('assets/plugins/bootstrap-datatable/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-datatable/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-datatable/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-datatable/js/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-datatable/js/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-datatable/js/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-datatable/js/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-datatable/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-datatable/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-datatable/js/buttons.colVis.min.js') }}"></script>

    <script>
            $(document).ready(function() {
             //Default data table
              $('#default-datatable').DataTable();
       
       
              var table = $('#example').DataTable( {
               lengthChange: false,
               buttons: [ 'copy', 'excel', 'pdf', 'print', 'colvis' ]
             } );
        
            table.buttons().container()
               .appendTo( '#example_wrapper .col-md-6:eq(0)' );
             
             } );
       
           </script>
@endsection

