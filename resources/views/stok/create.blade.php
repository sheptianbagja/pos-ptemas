@extends('layouts.master')

@section('content')
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <form id="signupForm" action="{{ route('stock.store') }}" method="POST">
                    @csrf
                <h4 class="form-header text-uppercase">
                  <i class="fa fa-bars"></i>
                   Tambah Data Stok
                </h4>

                @if (session('status'))
                <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <div class="alert-icon contrast-alert">
                         <i class="icon-check"></i>
                        </div>
                        <div class="alert-message">
                          <span><strong>Success!</strong> {{ session('status') }}</span>
                        </div>
                      </div> 
                @endif
                
                <div class="form-group row">
                        <label for="input-11" class="col-sm-2 col-form-label">Nama Produk</label>
                        <div class="col-sm-4">
                            <select name="product_id" class="form-control single-select" id="basic-select">
                                @foreach($products as $product)
                                    <option value="{{ $product->id }}">{{ ucwords($product->name) }}</option>
                                @endforeach
                            </select>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="input-12" class="col-sm-2 col-form-label">Stok</label>
                        <div class="col-sm-4">
                          <input type="text" class="form-control" id="input-12" name="stok">
                        </div>
                      </div>

                    <div class="form-footer">
                            <button type="submit" class="btn btn-danger"><i class="fa fa-times"></i> CANCEL</button>
                            <button type="submit" class="btn btn-success"><i class="fa fa-check-square-o"></i> SAVE</button>
                    </div>
                        
                    </form>
                  </div>
                </div>
              </div>
            </div><!--End Row-->

@endsection

@section('top')
  <link href="{{ asset('assets/plugins/select2/css/select2.min.css') }}" rel="stylesheet" />
@endsection

@section('bot')

<script src="{{  asset('assets/plugins/select2/js/select2.min.js') }}"></script>

<script>
    $(document).ready(function() {
        $('.single-select').select2();
      });
</script>

<!--Form Validatin Script-->
<script src="{{ asset('assets/plugins/jquery-validation/js/jquery.validate.min.js') }}"></script>
<script>

        $().ready(function() {
    
        // $("#form-stok").validate();
    
       // validate signup form on keyup and submit
        $("#signupForm").validate({
            rules: {
                product_id: "required",
                stok: "required",
            },
            messages: {
                product_id: "Mohon, masukan data produk",
                stok: "Mohon, masukan stok produk",
            }
        });
    
    });
    
        </script>

@endsection

