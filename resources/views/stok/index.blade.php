@extends('layouts.master')

@section('content')
<div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header"><i class="fa fa-table"></i>Data Stok</div>
            <div class="card-body">
                    <a href="{{ route('stock.create') }}"><button type="button" class="btn btn-primary waves-effect waves-light m-1">Tambah Data</button></a><br><br>
              
                     <form action="{{ route('stock.index')}}">

                            <div class="form-group row">
                                    <div class="col-sm-5">
                                        <select name="product_id" class="form-control" id="basic-select">
                                          <option value="">Pilih Produk</option>  
                                          @foreach($products as $p)
                                                <option value="{{ $p->id }}">{{ ucwords($p->name) }}</option>
                                            @endforeach
                                        </select>
          
                                    </div> 
                                    <div class="col-sm-5">
                                        <input 
                                        type="text" 
                                        class="form-control" 
                                        placeholder="cari berdasarkan stok"
                                        value="{{Request::get('stok')}}"
                                        name="stok">
      
                                    </div>
                                    <div class="col-sm-2">
                                            <input 
                                            type="submit" 
                                            value="Search / Filter" 
                                            class="btn btn-primary">
                                    </div>
                                  </div>
                    
                          </form> 

                          @if (session('status'))
                          <div class="alert alert-success alert-dismissible" role="alert">
                                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                                  <div class="alert-icon contrast-alert">
                                   <i class="icon-check"></i>
                                  </div>
                                  <div class="alert-message">
                                    <span><strong>Success!</strong> {{ session('status') }}</span>
                                  </div>
                                </div> 
                          @endif
                          
              
                    <div class="table-responsive">
              <table  class="table table-hover">
                <thead class="thead-info shadow-info">
                    <tr>
                        <th>ID</th>
                        <th>Product</th>
                        <th>Stock</th>
                        {{-- <th>Created At</th>
                        <th>Updated At</th> --}}
                        <th width="20px"></th>
                        <th width="70px"></th>
                    </tr>
                </thead>
                <tbody>
                  @php
                      $no = 1;
                  @endphp
                    @foreach ($stocks as $stock)
                    <tr>
                        <td>{{ $no++ }}</td>
                        <td>{{ ucwords($stock->product->name) }}</td>
                        <td>{{ $stock->stok }}</td>
                        {{-- <td>{{ $stock->created_at }}</td>
                        <td>{{ $stock->updated_at }}</td> --}}
                       <td>
                            <a href="{{ route('stock.edit', ['id' => $stock->id]) }}" class="btn btn-gradient-purpink btn-sm">Ubah</a>
                        </td>
                            <td>
                                    <form 
                                    class="d-inline"
                                    action="{{route('stock.destroy', ['id' => $stock->id])}}"
                                    method="POST"
                                    onsubmit="return confirm('Anda yakin akan menghapus data ini ?')"
                                    >
                  
                                    @csrf 
                  
                                    <input 
                                      type="hidden" 
                                      value="DELETE" 
                                      name="_method">
                  
                                    <input 
                                      type="submit" 
                                      class="btn btn-gradient-ibiza btn-sm" 
                                      value="Hapus">
                  
                                  </form>
                            </td>
                    <tr>
                    @endforeach
                </tbody>
                {{-- <tfoot class="thead-danger shadow-danger">
                        <th>ID</th>
                        <th>Name</th>
                        <th>Slug</th>
                        <th>Created At</th>
                        <th>Updated At</th>
                </tfoot> --}}
                </table>
                <br>
                {{ $stocks->links() }}
            </div>
            </div>
          </div>
        </div>
      </div><!-- End Row-->
@endsection

@section('top')
    <!--Data Tables -->
    <link href="{{ asset('assets/plugins/bootstrap-datatable/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/plugins/bootstrap-datatable/css/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css">
@endsection

@section('bot')
<!--Data Tables js-->
    <script src="{{ asset('assets/plugins/bootstrap-datatable/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-datatable/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-datatable/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-datatable/js/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-datatable/js/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-datatable/js/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-datatable/js/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-datatable/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-datatable/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-datatable/js/buttons.colVis.min.js') }}"></script>

    <script>
            $(document).ready(function() {
             //Default data table
              $('#default-datatable').DataTable();
       
       
              var table = $('#example').DataTable( {
               lengthChange: false,
               buttons: [ 'copy', 'excel', 'pdf', 'print', 'colvis' ]
             } );
        
            table.buttons().container()
               .appendTo( '#example_wrapper .col-md-6:eq(0)' );
             
             } );
       
           </script>
@endsection

