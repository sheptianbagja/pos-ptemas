<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

# Point Of Sales

## Note
Diharapkan agar import langsung file <b>ptemas.sql</b> karena sudah terdapat banyak data dummy.

## Langkah Penginstalan Projek
- clone repositori 
- buka code editor anda
- ketik <b>composer update</b>
- copy .env.example menjadi .env di root projek
- <b>php artisan key:generate</b>, untuk mendapatkan key projek
- configurasi database dan server local anda 
- php artisan serve
- done :)

## Login
- email : admin@gmail.com
- password : password

- Atau bisa lakukan register.
- 

## Fitur 
- Login
- Register
- Logout
- Dashboard
- Data Produk (CRUD,Search,Filter,Pagination)
- Data Kategori Produk (CRUD,Search,Filter,Pagination)
- Data Stok (CRUD,Search,Filter,Pagination)
- Fitur Transaksi (Create & List Transaksi (Search,Filter,Pagination) Metode Bayar, Diskon)
- Fitur Detail Transaksi(View)
- Fitur Refund (Create&List Refund(Search,Filter,Pagination))


## Beberapa Preview Screen Shoot Aplikasi
- Login ![](screenshoot/12.jpg)
- Register ![](screenshoot/14.jpg)
- Dashboard ![](screenshoot/1.jpg)
- Read List Produk![](screenshoot/2.jpg)
- Create produk ![](screenshoot/3.jpg)
- Read List Kategori ![](screenshoot/4.jpg)
- Create Kategori ![](screenshoot/5.jpg)
- Read List Stok ![](screenshoot/6.jpg)
- Create Stok ![](screenshoot/7.jpg)
- Read List Transaksi ![](screenshoot/8.jpg)
- View Detail Transaksi ![](screenshoot/13.jpg)
- Create Transaksi ![](screenshoot/9.jpg)
- Read List Refund ![](screenshoot/10.jpg)
- Create Refund ![](screenshoot/11.jpg)

