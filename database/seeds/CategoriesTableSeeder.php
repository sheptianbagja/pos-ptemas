<?php

use Illuminate\Database\Seeder;
use App\Category;
use Illuminate\Support\Carbon;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    DB::table('categories')->insert(array(
        [
            // 'id'            => 1,
            'name'          => 'baju pria',
            'slug'          => 'baju-pria',
            'created_at'    => Carbon::now()->format('Y-m-d H:i:s')
        ],
        [
            // 'id'            => 2,
            'name'      => 'baju wanita',
            'slug'      => 'baju-wanita',
            'created_at'    => Carbon::now()->format('Y-m-d H:i:s')
        ],
        [
            // 'id'            => 3,
            'name'      => 'handphone',
            'slug'      => 'handphone',
            'created_at'    => Carbon::now()->format('Y-m-d H:i:s')
        ],
        [
            // 'id'            => 4,
            'name'      => 'laptop',
            'slug'      => 'laptop',
            'created_at'    => Carbon::now()->format('Y-m-d H:i:s')
        ],
        [
            // 'id'            => 5,
            'name'      => 'kamera',
            'slug'      => 'kamera',
            'created_at'    => Carbon::now()->format('Y-m-d H:i:s')
        ],
        [
            // 'id'            => 6,
            'name'      => 'buku',
            'slug'      => 'buku',
            'created_at'    => Carbon::now()->format('Y-m-d H:i:s')
        ],
        [
            // 'id'            => 7,
            'name'      => 'jersey bola',
            'slug'      => 'software',
            'created_at'    => Carbon::now()->format('Y-m-d H:i:s')
        ],
        [
            // 'id'            => 8,
            'name'      => 'celana pria',
            'slug'      => 'celana pria',
            'created_at'    => Carbon::now()->format('Y-m-d H:i:s')
        ],
        [
            // 'id'            => 9,
            'name'      => 'celana wanita',
            'slug'      => 'celana wanit',
            'created_at'    => Carbon::now()->format('Y-m-d H:i:s')
        ],
        [
            // 'id'            => 10,
            'name'      => 'mainan anak',
            'slug'      => 'mainan-anak',
            'created_at'    => Carbon::now()->format('Y-m-d H:i:s')
        ],[
            // 'id'            => 11,
            'name'      => 'figur',
            'slug'      => 'figur',
            'created_at'    => Carbon::now()->format('Y-m-d H:i:s')
        ],
        [
            'name'      => 'gundam',
            'slug'      => 'gundam',
            'created_at'    => Carbon::now()->format('Y-m-d H:i:s')
        ],
        [
            'name'      => 'alat kesehatan',
            'slug'      => 'alat-kesehatan',
            'created_at'    => Carbon::now()->format('Y-m-d H:i:s')
        ],
        [
            'name'      => 'snack',
            'slug'      => 'snack',
            'created_at'    => Carbon::now()->format('Y-m-d H:i:s')
        ],
        [
            'name'      => '',
            'slug'      => 'gundam',
            'created_at'    => Carbon::now()->format('Y-m-d H:i:s')
        ],
    ));
    }
}
