<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $fillable = ['tanggal','nama_customer','alamat','handphone','metode_pembayaran','total','total_bayar','kembalian'];

    public function detail_transactions()
    {
        // return $this->hasMany(DetailTransaction::class);
        return $this->hasMany(DetailTransaction::class);
    }

    public function getProduct()
    {
        return $this->hasManyThrough(Product::class,DetailTransaction::class);
    }
}
