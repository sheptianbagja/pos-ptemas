<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Refund extends Model
{
    protected $fillable = ['transaction_id','note'];

    public function transaction()
    {
        return $this->belongsTo(Transaction::class);
    }

    // public static function GetTransaction($query)
    // {
    //     // return $transactions = Transaction::get();
    //     return $query->transaction->all();
    // }
}
