<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailTransaction extends Model
{
    // public $table = 'detail_transactions';
    protected $guards = [];

    public function transactions()
    {
        return $this->belongsTo(Transaction::class);
    }

    public function products()
    {
        return $this->belongsTo(Product::class);
    }

    public function product($id)
    {
        return Product::where('id',$id)->first();
    }
}
