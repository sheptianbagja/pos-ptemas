<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Charts;
use DB;
use App\Transaction;
use App\Refund;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $transactions = Transaction::where(DB::raw("(DATE_FORMAT(created_at,'%Y'))"),date('Y'))
    				->get();
        $chart = Charts::database($transactions, 'bar', 'highcharts')
			      ->title("Transaksi Perbulan")
			      ->elementLabel("Total Transaksi")
			      ->dimensions(500, 500)
			      ->responsive(false)
                  ->groupByMonth(date('Y'), true);

        $refunds = Refund::where(DB::raw("(DATE_FORMAT(created_at,'%Y'))"),date('Y'))
                  ->get();
        $chart_refunds = Charts::database($refunds, 'bar', 'highcharts')
                    ->title("Refund Perbulan")
                    ->elementLabel("Total Refund")
                    ->dimensions(500, 500)
                    ->responsive(false)
                    ->groupByMonth(date('Y'), true);
                  
        $title = "POS | Dashboard";
        return view('home',compact('title','chart','chart_refunds'));
    }
}
