<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transaction;
use App\Product;
use Mockery\Exception;
use App\DetailTransaction;
use DB;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $title = "POST | Data Transaksi";
        $customers = Transaction::orderBy('nama_customer','asc')->get();
        $transactions = Transaction::orderBy('id','desc')->paginate(10);

        $tanggal = $request->get('tanggal');
        $customer = $request->get('nama_customer');
        $metode_pembayaran = $request->get('metode_pembayaran');

        if ($tanggal) {
            $transactions = Transaction::where('tanggal',$tanggal)->orderBy('id','desc')->paginate(10);
        } elseif($customer) {
            $transactions = Transaction::where('nama_customer','LIKE',"%$customer%")->orderBy('id','desc')->paginate(10);
        } elseif($metode_pembayaran) {
            $transactions = Transaction::where('metode_pembayaran',$metode_pembayaran)->orderBy('id','desc')->paginate(10);
        }

        if ($tanggal && $customer) {
            $transactions = Transaction::where('tanggal',$tanggal)
            ->where('nama_customer','LIKE',"%$customer%")
            ->orderBy('id','desc')->paginate(10);
        } elseif ($tanggal && $metode_pembayaran) {
            $transactions = Transaction::where('tanggal',$tanggal)
            ->where('metode_pembayaran',$metode_pembayaran)
            ->orderBy('id','desc')->paginate(10);
        } elseif ($customer && $metode_pembayaran) {
            $transactions = Transaction::where('nama_customer','LIKE',"%$customer%")
            ->where('metode_pembayaran',$metode_pembayaran)
            ->orderBy('id','desc')->paginate(10);
        } 

        if ($tanggal && $customer && $metode_pembayaran) {
            $transactions = Transaction::where('tanggal',$tanggal)
            ->where('nama_customer','LIKE',"%$customer%")
            ->where('metode_pembayaran',$metode_pembayaran)
            ->orderBy('id','desc')->paginate(10);
        }

        return view('transaksi.index',compact('title','transactions','customers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = "POS | Buat Transaksi";
        $products = Product::where('stok','>=',1)->where('status','publish')->orderBy('name','asc')->get();
        return view('transaksi.create',compact('products','title'));

        // $barang = Product::all()->pluck('name','id');
        // return view('transaksi.create')
        // ->with('title',$title)
        // ->with('barang',$barang);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try{
            $input = $request->all();
            $transaction = Transaction::create($input);

            foreach($input['kode'] as $key => $row){
                $detail_transaction = new DetailTransaction();
                $product = Product::where('id', $input['kode'][$key])->first();

                $detail_transaction->product_id = $product->id;
                $detail_transaction->qty = $input['qty'][$key];
                $detail_transaction->diskon = $input['diskon'][$key];
                $detail_transaction->subtotal = $input['subtotal'][$key];
                $detail_transaction->transaction_id = $transaction->id;
                $detail_transaction->save();

                $new_stok = (int)$product->stok - (int)$input['qty'][$key];
                $product->stok = $new_stok;
                $product->save();
            }
            $result = $transaction->id;
            DB::commit();
        } catch(Exception $e) {
            DB::rollback();
        }

        // dd($result);
        return redirect()->back()->with('status','Anda berhasil membuat transaksi');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $title = "POS | Data Detail Transaksi";
        $transaction = Transaction::findOrFail($id);
        return view('transaksi.show',compact('transaction','title'));
    }

    public function search($id)
    {
        $transaction = Transaction::findOrFail($id);
        return $transaction;
    }
}
