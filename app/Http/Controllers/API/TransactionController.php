<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Transaction;
use DB;
use App\Product;
use App\DetailTransaction;
use Mockery\Exception;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $transactions = Transaction::with('detail_transactions')->orderBy('id','desc')->paginate(10);

        $tanggal = $request->get('tanggal');
        $customer = $request->get('nama_customer');
        $metode_pembayaran = $request->get('metode_pembayaran');

        
        if ($tanggal) {
            $transactions = Transaction::with('detail_transactions')->where('tanggal',$tanggal)->orderBy('id','desc')->paginate(10);

            $response = [
                'success'       => true,
                'msg'           => 'Berhasil mendapatkan filter data transaksi',
                'transactions'  => $transactions
            ];

            return response()->json($response, 200);

        } elseif($customer) {
            $transactions = Transaction::with('detail_transactions')->where('nama_customer','LIKE',"%$customer%")->orderBy('id','desc')->paginate(10);

            $response = [
                'success'       => true,
                'msg'           => 'Berhasil mendapatkan filter data transaksi',
                'transactions'  => $transactions
            ];

            return response()->json($response, 200);

        } elseif($metode_pembayaran) {
            $transactions = Transaction::with('detail_transactions')->where('metode_pembayaran',$metode_pembayaran)->orderBy('id','desc')->paginate(10);

            $response = [
                'success'       => true,
                'msg'           => 'Berhasil mendapatkan filter data transaksi',
                'transactions'  => $transactions
            ];

            return response()->json($response, 200);
        }

        if ($tanggal && $customer) {
            $transactions = Transaction::with('detail_transactions')->where('tanggal',$tanggal)
            ->where('nama_customer','LIKE',"%$customer%")
            ->orderBy('id','desc')->paginate(10);
            
            $response = [
                'success'       => true,
                'msg'           => 'Berhasil mendapatkan filter data transaksi',
                'transactions'  => $transactions
            ];

            return response()->json($response, 200);

        } elseif ($tanggal && $metode_pembayaran) {
            $transactions = Transaction::with('detail_transactions')->where('tanggal',$tanggal)
            ->where('metode_pembayaran',$metode_pembayaran)
            ->orderBy('id','desc')->paginate(10);

            $response = [
                'success'       => true,
                'msg'           => 'Berhasil mendapatkan filter data transaksi',
                'transactions'  => $transactions
            ];

            return response()->json($response, 200);

        } elseif ($customer && $metode_pembayaran) {
            $transactions = Transaction::with('detail_transactions')->where('nama_customer','LIKE',"%$customer%")
            ->where('metode_pembayaran',$metode_pembayaran)
            ->orderBy('id','desc')->paginate(10);

            $response = [
                'success'       => true,
                'msg'           => 'Berhasil mendapatkan filter data transaksi',
                'transactions'  => $transactions
            ];

            return response()->json($response, 200);
        } 

        

        if ($tanggal && $customer && $metode_pembayaran) {
            $transactions = Transaction::with('detail_transactions')->where('tanggal',$tanggal)
            ->where('nama_customer','LIKE',"%$customer%")
            ->where('metode_pembayaran',$metode_pembayaran)
            ->orderBy('id','desc')->paginate(10);

            $response = [
                'success'       => true,
                'msg'           => 'Berhasil mendapatkan filter data transaksi',
                'transactions'  => $transactions
            ];

            return response()->json($response, 200);
        } 

        $response = [
            'success'       => true,
            'msg'           => 'Berhasil mendapatkan data list transaksi',
            'transactions'  => $transactions
        ];

        return response()->json($response,200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try{
            $input = $request->all();
            $transaction = Transaction::create($input);

            foreach($input['product_id'] as $key => $row){
                $detail_transaction = new DetailTransaction();
                $product = Product::where('id', $input['product_id'][$key])->first();

                $detail_transaction->product_id = $product->id;
                $detail_transaction->qty = $input['qty'][$key];
                $detail_transaction->diskon = $input['diskon'][$key];
                $detail_transaction->subtotal = $input['subtotal'][$key];
                $detail_transaction->transaction_id = $transaction->id;
                $detail_transaction->save();

                $new_stok = (int)$product->stok - (int)$input['qty'][$key];
                $product->stok = $new_stok;
                $product->save();
            }
            $result = $transaction->id;
            DB::commit();
        } catch(Exception $e) {
            DB::rollback();
        }

        $response = [
            'success'       => true,
            'msg'           => 'Berhasil menambahkan data transaksi',
            'data'          => [
                'transaction'           => $transaction,
                'detail_transaction'    => $detail_transaction,
                'product'               => $product
            ]
        ];

        return response()->json($response, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $transaction = Transaction::with('detail_transactions')->findOrFail($id)->with('');
        $transaction = DetailTransaction::with('transactions')::with('products')->findOrFail($id);

        // $detail_transaction  = DetailTransaction::with('transactions')->findOrfail($id);

        $response = [
            'success'       => true,
            'msg'           => 'Berhasil mendapatkan data detail transaksi',
            'transaction'   => $transaction
        ];

        return response()->json($response, 200);
    }

    public function search($id) {
        $transaction = Transaction::with('detail_transactions')->findOrFail($id);

        $response = [
            'success'       => true,
            'msg'           => 'Berhasil mendapatkan data',
            'transaction'   => $transaction
        ];

        return response()->json($response, 200);
    }
}
