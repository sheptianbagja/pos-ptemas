<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Stock;
use App\Product;

class StockController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $stocks = Stock::with('product')->paginate(10);

        $product_search = $request->get('product_id');
        $stok = $request->get('stok');

        if ($product_search && $stok) {
            $stocks = Stock::where('product_id', $product_search)
            ->where('stok', $stok)
            ->orderBy('id','desc')->paginate(10);

            $response = [
                'success'   => true,
                'msg'       => 'Berhasil mendapatkan data filter stok',
                'stocks'    => $stocks
            ];

            return response()->json($response, 200);
        } else {
            $response = [
                'success'   => true,
                'msg'       => 'Berhasil mendapatkan data filter stok',
                'stocks'    => $stocks
            ];

            return response()->json($response, 200);
        }

        if ($stok) {
            $stocks = Stock::with('product')->where('stok',$stok)->orderBy('id','desc')->paginate(10);
            $response = [
                'success'   => true,
                'msg'       => 'Berhasil mendapatkan data filter stok',
                'stocks'    => $stocks
            ];

            return response()->json($response, 200);
        } else {
            $stocks = Stock::orderBy('id','desc')->paginate(10);

            $response = [
                'success'   => true,
                'msg'       => 'Berhasil mendapatkan data filter stok',
                'stocks'    => $stocks
            ];

            return response()->json($response, 200);
        }

        if($product_search){
            $stocks = Stock::with('product')->where('product_id', $product_search)->orderBy('id','desc')->paginate(10);

            $response = [
                'success'   => true,
                'msg'       => 'Berhasil mendapatkan data filter stok',
                'stocks'    => $stocks
            ];

            return response()->json($response, 200);
        } elseif ($stok) {
            $stocks = Stock::where('stok', $stok)->orderBy('id','desc')->paginate(10);

            $response = [
                'success'   => true,
                'msg'       => 'Berhasil mendapatkan data filter stok',
                'stocks'    => $stocks
            ];

            return response()->json($response, 200);
        }

        $response = [
            'success'   => true,
            'msg'       => 'Berhasil mendapatkan data stok',
            'stocks'    => $stocks
        ];

        return response()->json($response, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'product_id'    => 'required',
            'stok'          => 'required'
        ]);

        $product_id = $request->get('product_id');
        $stok_isi       = $request->get('stok');

        $stok = new Stock();
        $stok->product_id = $product_id;
        $stok->stok       = $stok_isi;
        $stok->save();

        $product = Product::findOrFail($request->get('product_id'));
        $product->stok += $request->get('stok');
        $product->save();

        $response = [
            'success'           => true,
            'msg'               => 'Berhasil menambahkan data stok',
            'stok'              => $stok,
            'product'           => $product,
            'detail_product'    => [
                'href'      => 'api/products/'.$product_id,
                'method'    => 'GET'
            ]
        ];

        return response()->json($response, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $stock = Stock::with('product')->findOrFail($id);

        $response = [
            'success'   => true,
            'msg'       => 'berhasil mendapatkan data detail stok',
            'stock'     => $stock
        ];

        return response()->json($response, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'product_id'    => 'required',
            'stok'          => 'required'
        ]);

        $product_id = $request->get('product_id');
        $stok_isi       = $request->get('stok');

        $stok = Stock::findOrFail($id);
        $stok->product_id = $product_id;
        $stok->stok       = $stok_isi;
        $stok->update();

        $product = Product::findOrFail($request->get('product_id'));
        $product->stok += $request->get('stok');
        $product->update();

        $response = [
            'success'           => true,
            'msg'               => 'Berhasil menambahkan data stok',
            'stok'              => $stok,
            'product'           => $product,
            'detail_product'    => [
                'href'      => 'api/products/'.$product_id,
                'method'    => 'GET'
            ]
        ];

        return response()->json($response, 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $stock = Stock::findOrFail($id);
        $stock->delete();

        $response = [
            'success'   => true,
            'msg'       => 'Berhasil menghapus data stok'
        ];

        return response()->json($response, 200);
    }
}
