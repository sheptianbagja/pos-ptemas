<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Refund;
use App\Transaction;

class RefundController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $refunds = Refund::with('transaction')->paginate(10);

        $tanggal = $request->get('tanggal');
        $transaction_id = $request->get('transaction_id');
        $customer = $request->get('nama_customer');

        if ($tanggal) {
            $refunds = Refund::with('transaction')->where('tanggal',$tanggal)->orderBy('id','desc')->paginate(10);

            $response = [
                'success'   => true,
                'msg'       => 'Berhasil mendapatkan data',
                'refunds'   => $refunds
            ];
    
            return response()->json($response, 200);

        } elseif($customer) {
            $refunds = Refund::with('transaction')->where('nama_customer','LIKE',"%$customer%")->orderBy('id','desc')->paginate(10);

            $response = [
                'success'   => true,
                'msg'       => 'Berhasil mendapatkan data',
                'refunds'   => $refunds
            ];
    
            return response()->json($response, 200);

        } elseif($transaction_id) {
            $refunds = Refund::with('transaction')->where('transaction_id',$transaction_id)->orderBy('id','desc')->paginate(10);

            $response = [
                'success'   => true,
                'msg'       => 'Berhasil mendapatkan data',
                'refunds'   => $refunds
            ];
    
            return response()->json($response, 200);

        }

        if ($tanggal && $customer) {
            $refunds = Refund::with('transaction')->where('tanggal',$tanggal)
            ->where('nama_customer','LIKE',"%$customer%")
            ->orderBy('id','desc')->paginate(10);

            $response = [
                'success'   => true,
                'msg'       => 'Berhasil mendapatkan data',
                'refunds'   => $refunds
            ];
    
            return response()->json($response, 200);

        } elseif ($tanggal && $transaction_id) {
            $refunds = Refund::with('transaction')->where('tanggal',$tanggal)
            ->where('transaction_id',$transaction_id)
            ->orderBy('id','desc')->paginate(10);

            $response = [
                'success'   => true,
                'msg'       => 'Berhasil mendapatkan data',
                'refunds'   => $refunds
            ];
    
            return response()->json($response, 200);

        } elseif ($customer && $transaction_id) {
            $refunds = Refund::with('transaction')->where('nama_customer','LIKE',"%$customer%")
            ->where('transaction_id',$transaction_id)
            ->orderBy('id','desc')->paginate(10);

            $response = [
                'success'   => true,
                'msg'       => 'Berhasil mendapatkan data',
                'refunds'   => $refunds
            ];
    
            return response()->json($response, 200);

        } 

        if ($tanggal && $customer && $transaction_id) {
            $refunds = Refund::with('transaction')->where('tanggal',$tanggal)
            ->where('nama_customer','LIKE',"%$customer%")
            ->where('transaction_id',$transaction_id)
            ->orderBy('id','desc')->paginate(10);

            $response = [
                'success'   => true,
                'msg'       => 'Berhasil mendapatkan data',
                'refunds'   => $refunds
            ];
    
            return response()->json($response, 200);
        }

        $response = [
            'success'   => true,
            'msg'       => 'Berhasil mendapatkan data',
            'refunds'   => $refunds
        ];

        return response()->json($response, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'transaction_id'    => 'required',
            'note'              => 'required'
        ]);

        $transaction_id = $request->get('transaction_id');
        $tanggal        = $request->get('tanggal');
        $note           = $request->get('note');
        $nama_customer  = $request->get('nama_customer');

        $refund = new Refund();
        $refund->transaction_id = $transaction_id;
        $refund->tanggal        = $tanggal;
        $refund->note           = $note;
        $refund->nama_customer  = $nama_customer;
        $refund->save();

        $transaction = Transaction::findOrfail($transaction_id);
        $transaction->status = 'refund';
        $transaction->save();

        $response = [
            'success'       => true,
            'msg'           => 'Berhasil menambahkan data refund',
            'refund'        => $refund,
            'transaction'   => $transaction,
        ];

        return response()->json($response, 201);

    }

}
