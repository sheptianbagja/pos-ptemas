<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\Category;
use App\Traits\UploadTrait;

class ProductController extends Controller
{
    use UploadTrait;

    public function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $products = Product::with('category')->paginate(10);

        $filterKeyword = $request->get('name');
        $status=$request->get('status');
        $category_id=$request->get('category_id');
        $min_price=$request->get('min_price');
        $max_price=$request->get('max_price');

        if($filterKeyword){
            $products = Product::where('name', 'LIKE', "%$filterKeyword%")
                ->orderBy('id','desc')->paginate(10);

            $response = [
                'success'   => true,
                'msg'       => 'Berhasil mendapatkan data produk',
                'products'  => $products
            ];
    
            return response()->json($response, 200);
                
        } elseif($min_price){
            $products = Product::where('price','>=',$min_price)
                ->orderBy('id','desc')->paginate(10);

            $response = [
                'success'   => true,
                'msg'       => 'Berhasil mendapatkan data produk',
                'products'  => $products
            ];
    
            return response()->json($response, 200);

        } elseif($max_price){
            $products = Product::where('price','<=',$max_price)
                ->orderBy('id','desc')->paginate(10);

            $response = [
                'success'   => true,
                'msg'       => 'Berhasil mendapatkan data produk',
                'products'  => $products
            ];
    
            return response()->json($response, 200);

        } elseif ($status) {
            $products = Product::where('status', $status)->orderBy('id','desc')->paginate(10);

            $response = [
                'success'   => true,
                'msg'       => 'Berhasil mendapatkan data produk',
                'products'  => $products
            ];
    
            return response()->json($response, 200);

        } elseif ($category_id) {
            $products = Product::where('category_id', $category_id)->orderBy('id','desc')->paginate(10);

            $response = [
                'success'   => true,
                'msg'       => 'Berhasil mendapatkan data produk',
                'products'  => $products
            ];
    
            return response()->json($response, 200);

        } else {
            $products = Product::orderBy('id','desc')->paginate(10);

            $response = [
                'success'   => true,
                'msg'       => 'Berhasil mendapatkan data produk',
                'products'  => $products
            ];
    
            return response()->json($response, 200);
        }

        if ($filterKeyword && $min_price) {
            $products = Product::where('name', 'LIKE', "%$filterKeyword%")
                ->where('price','>=',$min_price)
                ->orderBy('id','desc')->paginate(10);

                $response = [
                    'success'   => true,
                    'msg'       => 'Berhasil mendapatkan data produk',
                    'products'  => $products
                ];
        
                return response()->json($response, 200);
        }

        if ($filterKeyword && $max_price) {
            $products = Product::where('name', 'LIKE', "%$filterKeyword%")
                    ->where('price','<=',$max_price)
                    ->orderBy('id','desc')->paginate(10);
            
            $response = [
                'success'   => true,
                'msg'       => 'Berhasil mendapatkan data produk',
                'products'  => $products
            ];
            
            return response()->json($response, 200);
        }

        if ($filterKeyword && $status) {
            $products = Product::where('name', 'LIKE', "%$filterKeyword%")
                    ->where('status', $status)->orderBy('id','desc')->paginate(10);
            
            $response = [
                'success'   => true,
                'msg'       => 'Berhasil mendapatkan data produk',
                'products'  => $products
            ];
                    
            return response()->json($response, 200);
        }

        if ($filterKeyword && $category_id) {
            $products = Product::where('name', 'LIKE', "%$filterKeyword%")
                    ->where('category_id', $category_id)->orderBy('id','desc')->paginate(10);
            
            $response = [
                'success'   => true,
                'msg'       => 'Berhasil mendapatkan data produk',
                'products'  => $products
            ];
        }

        if ($min_price && $max_price) {
            $products = Product::where('price','>=',$min_price)
                    ->where('price','<=',$max_price)
                    ->orderBy('id','desc')->paginate(10);

            $response = [
                'success'   => true,
                'msg'       => 'Berhasil mendapatkan data produk',
                'products'  => $products
            ];
        }

        if ($min_price && $status) {
            $products = Product::where('price','>=',$min_price)
                    ->where('status', $status)->orderBy('id','desc')->paginate(10);

            $response = [
                'success'   => true,
                'msg'       => 'Berhasil mendapatkan data produk',
                'products'  => $products
            ];
        }

        if ($min_price && $category_id) {
            $products = Product::where('price','>=',$min_price)
                    ->where('category_id', $category_id)->orderBy('id','desc')->paginate(10);

            $response = [
                'success'   => true,
                'msg'       => 'Berhasil mendapatkan data produk',
                'products'  => $products
            ];
        }

        if ($max_price && $status) {
            $products = Product::where('price','<=',$max_price)	
                    ->where('status', $status)->orderBy('id','desc')->paginate(10);

            $response = [
                'success'   => true,
                'msg'       => 'Berhasil mendapatkan data produk',
                'products'  => $products
            ];
        }

        if ($max_price && $category_id) {
            $products = Product::where('price','<=',$max_price)	
                    ->where('category_id', $category_id)->orderBy('id','desc')->paginate(10);

            $response = [
                'success'   => true,
                'msg'       => 'Berhasil mendapatkan data produk',
                'products'  => $products
            ];
        }

        if ($status && $category_id) {
            $products = Product::where('category_id',$category_id)
                    ->where('category_id', $category_id)->orderBy('id','desc')->paginate(10);

            $response = [
                'success'   => true,
                'msg'       => 'Berhasil mendapatkan data produk',
                'products'  => $products
            ];
        }

        if ($filterKeyword && $min_price && $max_price) {
            $products = Product::where('name', 'LIKE', "%$filterKeyword%")
                ->where('price','>=',$min_price)
                ->where('price','<=',$max_price)
                ->orderBy('id','desc')->paginate(10);

            $response = [
                'success'   => true,
                'msg'       => 'Berhasil mendapatkan data produk',
                'products'  => $products
            ];
        }

        if ($filterKeyword && $min_price && $status) {
            $products = Product::where('name', 'LIKE', "%$filterKeyword%")
                ->where('price','>=',$min_price)
                ->where('status', $status)
                ->orderBy('id','desc')->paginate(10);

            $response = [
                'success'   => true,
                'msg'       => 'Berhasil mendapatkan data produk',
                'products'  => $products
            ];
        }

        if ($filterKeyword && $min_price && $category_id) {
            $products = Product::where('name', 'LIKE', "%$filterKeyword%")
                ->where('price','>=',$min_price)
                ->where('category_id', $category_id)
                ->orderBy('id','desc')->paginate(10);

            $response = [
                'success'   => true,
                'msg'       => 'Berhasil mendapatkan data produk',
                'products'  => $products
            ];
        }

        if ($min_price && $status && $category_id) {
            $products = Product::where('price','>=',$min_price)
                ->where('status', $status)
                ->where('category_id',$category_id)
                ->orderBy('id','desc')->paginate(10);

            $response = [
                'success'   => true,
                'msg'       => 'Berhasil mendapatkan data produk',
                'products'  => $products
            ];
        }

        if ($max_price && $status && $category_id) {
            $products = Product::where('price','<=',$max_price)	
                ->where('status', $status)
                ->where('category_id',$category_id)
                ->orderBy('id','desc')->paginate(10);

            $response = [
                'success'   => true,
                'msg'       => 'Berhasil mendapatkan data produk',
                'products'  => $products
            ];
        }

        if ($status && $min_price && $max_price) {
            $products = Product::where('status', $status)
                ->where('price','>=',$min_price)
                ->where('price','<=',$max_price)
                ->orderBy('id','desc')->paginate(10);

            $response = [
                'success'   => true,
                'msg'       => 'Berhasil mendapatkan data produk',
                'products'  => $products
            ];
        }

        if ($category_id && $filterKeyword && $max_price) {
            $products = Product::where('category_id',$category_id)
                ->where('name', 'LIKE', "%$filterKeyword%")
                ->where('price','<=',$max_price)
                ->orderBy('id','desc')->paginate(10);

            $response = [
                'success'   => true,
                'msg'       => 'Berhasil mendapatkan data produk',
                'products'  => $products
            ];
        }

        if ($filterKeyword && $min_price && $max_price && $status && $category_id) {
            $products = Product::where('name', 'LIKE', "%$filterKeyword%")
                        ->where('price','>=',$min_price)
                        ->where('price','<=',$max_price)
                        ->where('status', $status)
                        ->where('category_id',$category_id)
                        ->orderBy('id','desc')->paginate(10);

            $response = [
                'success'   => true,
                'msg'       => 'Berhasil mendapatkan data produk',
                'products'  => $products
            ];
        }

        $response = [
            'success'   => true,
            'msg'       => 'Berhasil mendapatkan data produk',
            'products'  => $products
        ];

        return response()->json($response, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function getDataCategories()
    {
        $category = Category::paginate(10);
        
        $response = [
            'success'   => true,
            'msg'       => 'Berhasil mendapatkan data kategori',
            'category'  => $category
        ];

        return response()->json($response,200);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'category_id'       => 'required',
            'name'              => 'required|string|min:3',
            'description'       => 'required|min:5',
            'price'             => 'required|min:3',
            'image'             => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'color'             => 'required|string|min:3',
            'status'            => 'required',
            // 'name'              => 'required|string|min:3',
        ]);

        $category_id = $request->get('category_id');
        $name        = $request->get('name');
        $description = $request->get('description');
        $price       = $request->get('price');
        $color       = $request->get('color');
        $status      = $request->get('status');

        $product = new Product();
        $product->category_id   = $category_id;
        $product->name          = $name;
        $product->description   = $description;
        $product->price         = $price;
        $product->color         = $color;
        $product->status        = $status;         

        if ($request->has('image')) {
            $image = $request->file('image');
            $name = str_slug($request->input('name'),'_').'_'.str_random(8);
            $folder = '/uploads/images/';
            $filePath = $folder . $name.'.'.$image->getClientOriginalExtension();
            $this->uploadOne($image, $folder, 'public',$name);
            $product->image = $filePath;
        }

        $product->save();

        $category = Category::findOrFail($category_id);

        $response = [
            'success'       => true,
            'msg'           => 'Berhasil menambahkan produk',
            'product'       => $product,
            'category'      => $category,
            'view_category' => [
                'href'      => 'api/categories',
                'method'    => 'GET'
            ]
        ];

        return response()->json($response,201);


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::with('category')->findOrFail($id);

        $response = [
            'success'   => true,
            'msg'       => 'Berhasil mendapatkan data detail produk',
            'product'   => $product
        ];

        return response()->json($response, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'category_id'       => 'required',
            'name'              => 'required|string|min:3',
            'description'       => 'required|min:5',
            'price'             => 'required|min:3',
            // 'image'             => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'color'             => 'required|string|min:3',
            'status'            => 'required',
            // 'name'              => 'required|string|min:3',
        ]);

        $category_id = $request->get('category_id');
        $name        = $request->get('name');
        $description = $request->get('description');
        $price       = $request->get('price');
        $color       = $request->get('color');
        $status      = $request->get('status');

        // $product = new Product();
        $product = Product::findOrFail($id);
        $product->category_id   = $category_id;
        $product->name          = $name;
        $product->description   = $description;
        $product->price         = $price;
        $product->color         = $color;
        $product->status        = $status;         

        if ($request->has('image')) {
            if (!$product->image == NULL){
                unlink(public_path($product->image));
            }
            $image = $request->file('image');
            $name = str_slug($request->input('name'),'_').'_'.str_random(8);
            $folder = '/upload/products/';
            $filePath = $folder . $name.'.'.$image->getClientOriginalExtension();
            $this->uploadOne($image, $folder, 'public',$name);
            $product->image = $filePath;
        }

        $product->update();

        $category = Category::findOrFail($category_id);

        $response = [
            'success'       => true,
            'msg'           => 'Berhasil mengubah produk',
            'product'       => $product,
            'category'      => $category,
            'view_category' => [
                'href'      => 'api/categories',
                'method'    => 'GET'
            ]
        ];

        return response()->json($response,201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::findOrFail($id);

        if (!$product->image == NULL){
            unlink(public_path($product->image));
        }

        $product->delete();
        
        $response = [
            'success'   => true,
            'msg'       => 'Berhasil menghapus data produk',
            'product'   => $product
        ];

        return response()->json($response, 200);
    }
}
