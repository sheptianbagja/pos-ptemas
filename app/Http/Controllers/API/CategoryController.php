<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
// use Validator;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        $categories = Category::orderBy('id','desc')->paginate(10);
        $status = $request->get('status');
        $filterKeyword = $request->get('name');

        if ($status) {

            $categories = Category::where('status', $status)->orderBy('id','desc')->paginate(10);
            $response = [
                'success'       => true,
                'msg'           => 'Berhasil mendapatkan filter',
                'categories'    => $categories
            ];
            return response()->json($response,200);

        } else {
            $categories = Category::orderBy('id','desc')->paginate(10);
        }

        if($filterKeyword){
            if ($status) {
                $categories = Category::where('name', 'LIKE', "%$filterKeyword%")
                ->where('status', $status)
                ->orderBy('id','desc')->paginate(10);

                $response = [
                    'success'       => true,
                    'msg'           => 'Berhasil mendapatkan filter',
                    'categories'    => $categories
                ];
                return response()->json($response,200);

            } else {
                $categories = Category::where('name', 'LIKE', "%$filterKeyword%")
                ->orderBy('id','desc')->paginate(10);

                $response = [
                    'success'       => true,
                    'msg'           => 'Berhasil mendapatkan filter',
                    'categories'    => $categories
                ];
                return response()->json($response,200);
            }
        }

        $response = [
            'success'       => true,
            'msg'           => 'Berhasil mendapatkan data list kategori',
            'categories'    => $categories
        ];

        return response()->json($response,200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'  => 'required|string|min:3'
        ]);

        $name = $request->get('name');
        $slug = str_slug($request->get('name'),'-');

        $category = new Category([
            'name'     => $name,
            'slug'     => $slug
        ]);

        $category->save();

        $response = [
            'success'   => true,
            'msg'       => 'Berhasil menambahkan data kategori',
            'category'  => $category
        ];

        return  response()->json($response, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = Category::findOrFail($id);

        $response = [
            'success'   => true,
            'msg'       => 'Berhasil mendapatkan show data',
            'category'  =>  $category
        ];

        return response()->json($response, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'  => 'required|string|min:3'
        ]);

        $name = $request->get('name');
        $slug = str_slug($request->get('name'));

        $category = Category::findOrFail($id);
        $category->name = $name;
        $category->slug = $slug;
        // $category->update();

        if (!$category->update()) {
            return response()->json([
                'msg'   => 'Error during update'
            ], 404);
        }

        $response = [
            'success'   => true,
            'msg'       => 'Berhasil mengubah data kategori',
            'category'  => $category
        ];

        return response()->json($response, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::findOrFail($id);
        $category->delete();

        if (is_null($category)) {
            $response = [
                'msg' => 'data tidak ditemukan'
            ];    

            return response()->json($response, 404);
        }

        $response = [
            'success'   => true,
            'msg'       => 'Berhasil menghapus data kategori',
            'category'  => $category,
        ];

        return response()->json($response,200);
    }
}
