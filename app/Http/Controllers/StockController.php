<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Stock;
use App\Product;
use App\Category;
use App\HistoryStock;

class StockController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $title = "POST | Data Stok";
        $stocks = Stock::orderBy('id','desc')->paginate(10);
        $products = Product::orderBy('name','asc')->get();
        // $products = Product::orderBy('name','asc')->get();

        $product_search = $request->get('product_id');
        // $stok = $request->get('stok');

        

        // if ($stok && $product_search) {
        //     $stocks = Stock::where('stok',$stok)
        //     ->where('product_id',$product_search)
        //     ->paginate(10);
        // } else {
        //     $stocks = Stock::paginate(10);
        // }

        $stok=$request->get('stok');

        if($stok){
            $stocks = Stock::where('stok', $stok)->orderBy('id','desc')->paginate(10);
        } else {
            $stocks = Stock::orderBy('id','desc')->paginate(10);
        }

        if($product_search){
            $stocks = Stock::where('product_id', $product_search)->orderBy('id','desc')->paginate(10);
        } elseif ($stok) {
            $stocks = Stock::where('stok', $stok)->orderBy('id','desc')->paginate(10);
        }

        if ($product_search && $stok) {
            $stocks = Stock::where('product_id', $product_search)
            ->where('stok', $stok)
            ->orderBy('id','desc')->paginate(10);
        } 

        return view('stok.index',compact('stocks','products','title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = "POS | Tambah Data Stok";
        $products = Product::orderBy('name','asc')->get();
        return view('stok.create',compact('title','products'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'product_id'     => 'required',
            'stok'           => 'required',  
        ]);
        
        $stock = new Stock();
        $stock->product_id = $request->get('product_id');
        $stock->stok = $request->get('stok');
        // $stock->riwayat_stok = $request->get('stok');
        $stock->save();
        // Stock::create($request->all());

        // $history_stock = new HistoryStock();
        // $history_stock->stock_id    = $stock->id;
        // $history_stock->product_id  = $request->get('product_id');
        // $history_stock->stok       = $request->get('stok');
        // $history_stock->save();

        $product = Product::findOrFail($request->product_id);
        $product->stok += $request->stok;
        $product->save();

        return redirect()->back()->with('status','Anda berhasil menambahkan data stok');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title = 'POS| Edit Data Stok';
        $stock = Stock::findOrFail($id);
        $products = Product::orderBy('name','asc')->get();
        return view('stok.edit',compact('products','title','stock'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'product_id'     => 'required',
            'stok'           => 'required',  
        ]);

        $stock = Stock::findOrFail($id);
        $stock->product_id = $request->get('product_id');
        $stock->stok = $request->get('stok');
        $stock->update();

        // $history_stock = new HistoryStock();
        // $history_stock->stock_id    = $stock->id;
        // $history_stock->product_id  = $request->get('product_id');
        // $history_stock->stok       = $request->get('stok');
        // $history_stock->save();

    
        // $history = HistoryStock::where('product_id',$request->product_id);

        // $product_old = Product::findOrFail($request->product_id);
        // $product_old->stok -=  $history->stok;
        // $product_old->update();

        $product = Product::findOrFail($request->product_id);
        $product->stok -= $product->riwayat_stok;
        $product->update();

        // $product = Product::findOrFail($request->product_id);
        // $product->stok += $request->stok;
        // $product->update();

        return redirect()->back()->with('status','Anda berhasil mengubah data stok');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $stock = Stock::findOrFail($id);
        $stock->delete();
        return redirect()->back()->with('status','Anda berhasil menghapus data stok');
    }
}
