<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Refund;
use App\Transaction;
use App\Product;

class RefundController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $title = "POS | Lihat Data Refund";
        $refunds = Refund::orderBy('id','DESC')->paginate(10);

        $tanggal = $request->get('tanggal');
        $transaction_id = $request->get('transaction_id');
        $customer = $request->get('nama_customer');

        if ($tanggal) {
            $refunds = Refund::where('tanggal',$tanggal)->orderBy('id','desc')->paginate(10);
        } elseif($customer) {
            $refunds = Refund::where('nama_customer','LIKE',"%$customer%")->orderBy('id','desc')->paginate(10);
        } elseif($transaction_id) {
            $refunds = Refund::where('transaction_id',$transaction_id)->orderBy('id','desc')->paginate(10);
        }

        if ($tanggal && $customer) {
            $refunds = Refund::where('tanggal',$tanggal)
            ->where('nama_customer','LIKE',"%$customer%")
            ->orderBy('id','desc')->paginate(10);
        } elseif ($tanggal && $transaction_id) {
            $refunds = Refund::where('tanggal',$tanggal)
            ->where('transaction_id',$transaction_id)
            ->orderBy('id','desc')->paginate(10);
        } elseif ($customer && $transaction_id) {
            $refunds = Refund::where('nama_customer','LIKE',"%$customer%")
            ->where('transaction_id',$transaction_id)
            ->orderBy('id','desc')->paginate(10);
        } 

        if ($tanggal && $customer && $transaction_id) {
            $refunds = Refund::where('tanggal',$tanggal)
            ->where('nama_customer','LIKE',"%$customer%")
            ->where('transaction_id',$transaction_id)
            ->orderBy('id','desc')->paginate(10);
        }

        return view('refund.index', compact('title','refunds'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = "POS | Buat Refund";
        $transactions = Transaction::where('status','done')->orderBy('id','asc')->get();
        return view('refund.create',compact('title','transactions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'transaction_id'    => 'required',
            'note'              => 'required'
        ]);

        $refund = new Refund();
        $refund->transaction_id = $request->get('transaction_id');
        $refund->tanggal        = $request->get('tanggal');
        $refund->note           = $request->get('note');
        $refund->nama_customer  = $request->get('nama_customer');
        $refund->save();

        $transaction = Transaction::findOrFail($request->get('transaction_id'));
        $transaction->status  = 'refund';
        $transaction->save();

        return redirect()->back()->with('status','Anda berhasil melakukan refund transaksi');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
