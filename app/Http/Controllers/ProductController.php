<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Category;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $title = "POS | Data Produk";
        $products = Product::orderBy('id','desc')->paginate(10);
        $categories = Category::orderBy('name','asc')->get();

        $filterKeyword = $request->get('name');
        $status=$request->get('status');
        $category_id=$request->get('category_id');
        $min_price=$request->get('min_price');
        $max_price=$request->get('max_price');

        if($filterKeyword){
            $products = Product::where('name', 'LIKE', "%$filterKeyword%")
                ->orderBy('id','desc')->paginate(10);
        } elseif($min_price){
            $products = Product::where('price','>=',$min_price)
                ->orderBy('id','desc')->paginate(10);
        } elseif($max_price){
            $products = Product::where('price','<=',$max_price)
                ->orderBy('id','desc')->paginate(10);
        } elseif ($status) {
            $products = Product::where('status', $status)->orderBy('id','desc')->paginate(10);
        } elseif ($category_id) {
            $products = Product::where('category_id', $category_id)->orderBy('id','desc')->paginate(10);
        } else {
            $products = Product::orderBy('id','desc')->paginate(10);
        }

        if ($filterKeyword && $min_price) {
            $products = Product::where('name', 'LIKE', "%$filterKeyword%")
                ->where('price','>=',$min_price)
                ->orderBy('id','desc')->paginate(10);
        }

        if ($filterKeyword && $max_price) {
            $products = Product::where('name', 'LIKE', "%$filterKeyword%")
                    ->where('price','<=',$max_price)
                    ->orderBy('id','desc')->paginate(10);
        }

        if ($filterKeyword && $status) {
            $products = Product::where('name', 'LIKE', "%$filterKeyword%")
                    ->where('status', $status)->orderBy('id','desc')->paginate(10);
        }

        if ($filterKeyword && $category_id) {
            $products = Product::where('name', 'LIKE', "%$filterKeyword%")
                    ->where('category_id', $category_id)->orderBy('id','desc')->paginate(10);
        }

        if ($min_price && $max_price) {
            $products = Product::where('price','>=',$min_price)
                    ->where('price','<=',$max_price)
                    ->orderBy('id','desc')->paginate(10);
        }

        if ($min_price && $status) {
            $products = Product::where('price','>=',$min_price)
                    ->where('status', $status)->orderBy('id','desc')->paginate(10);
        }

        if ($min_price && $category_id) {
            $products = Product::where('price','>=',$min_price)
                    ->where('category_id', $category_id)->orderBy('id','desc')->paginate(10);
        }

        if ($max_price && $status) {
            $products = Product::where('price','<=',$max_price)	
                    ->where('status', $status)->orderBy('id','desc')->paginate(10);
        }

        if ($max_price && $category_id) {
            $products = Product::where('price','<=',$max_price)	
                    ->where('category_id', $category_id)->orderBy('id','desc')->paginate(10);
        }

        if ($status && $category_id) {
            $products = Product::where('category_id',$category_id)
                    ->where('category_id', $category_id)->orderBy('id','desc')->paginate(10);
        }

        if ($filterKeyword && $min_price && $max_price) {
            $products = Product::where('name', 'LIKE', "%$filterKeyword%")
                ->where('price','>=',$min_price)
                ->where('price','<=',$max_price)
                ->orderBy('id','desc')->paginate(10);
        }

        if ($filterKeyword && $min_price && $status) {
            $products = Product::where('name', 'LIKE', "%$filterKeyword%")
                ->where('price','>=',$min_price)
                ->where('status', $status)
                ->orderBy('id','desc')->paginate(10);
        }

        if ($filterKeyword && $min_price && $category_id) {
            $products = Product::where('name', 'LIKE', "%$filterKeyword%")
                ->where('price','>=',$min_price)
                ->where('category_id', $category_id)
                ->orderBy('id','desc')->paginate(10);
        }

        if ($min_price && $status && $category_id) {
            $products = Product::where('price','>=',$min_price)
                ->where('status', $status)
                ->where('category_id',$category_id)
                ->orderBy('id','desc')->paginate(10);
        }

        if ($max_price && $status && $category_id) {
            $products = Product::where('price','<=',$max_price)	
                ->where('status', $status)
                ->where('category_id',$category_id)
                ->orderBy('id','desc')->paginate(10);
        }

        if ($status && $min_price && $max_price) {
            $products = Product::where('status', $status)
                ->where('price','>=',$min_price)
                ->where('price','<=',$max_price)
                ->orderBy('id','desc')->paginate(10);
        }

        if ($category_id && $filterKeyword && $max_price) {
            $products = Product::where('category_id',$category_id)
                ->where('name', 'LIKE', "%$filterKeyword%")
                ->where('price','<=',$max_price)
                ->orderBy('id','desc')->paginate(10);
        }

        if ($filterKeyword && $min_price && $max_price && $status && $category_id) {
            $products = Product::where('name', 'LIKE', "%$filterKeyword%")
                        ->where('price','>=',$min_price)
                        ->where('price','<=',$max_price)
                        ->where('status', $status)
                        ->where('category_id',$category_id)
                        ->orderBy('id','desc')->paginate(10);
        }

        return view('produk.index', compact('title','products','categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = "POS | Tambah Data Produk";
        $categories = Category::orderBy('name','asc')->get();
        return view('produk.create',compact('title','categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'category_id'       => 'required',
            'name'              => 'required|string|min:3',
            'description'       => 'required|min:5',
            'price'             => 'required|min:3',
            'image'             => 'required',
            'color'             => 'required|string|min:3',
            'status'            => 'required',
            'name'              => 'required|string|min:3',
        ]);

        $input = $request->all();
        if ($request->hasFile('image')){
            $input['image'] = '/upload/products/'.str_slug($input['name'], '-').'.'.$request->image->getClientOriginalExtension();
            $request->image->move(public_path('/upload/products/'), $input['image']);
        }

        Product::create($input);
        
        return redirect()->back()->with('status','Anda berhasil menambahkan product');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title = 'POS | Edit Data Produk';
        $product = Product::findOrFail($id);
        $categories = Category::orderBy('name','asc')->get();
        return view('produk.edit', compact('product', 'title','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'category_id'       => 'required',
            'name'              => 'required|string|min:3',
            'description'       => 'required|min:5',
            'price'             => 'required|min:3',
            // 'image'             => 'required',
            'color'             => 'required|string|min:3',
            'status'            => 'required',
            'name'              => 'required|string|min:3',
        ]);

        $input = $request->all();
        $product = Product::findOrFail($id);
        $input['image'] = $product->image;

        if ($request->hasFile('image')){
            if (!$product->image == NULL){
                unlink(public_path($product->image));
            }
            $input['image'] = '/upload/products/'.str_slug($input['name'], '-').'.'.$request->image->getClientOriginalExtension();
            $request->image->move(public_path('/upload/products/'), $input['image']);
        }

        $product->update($input);
        return redirect()->back()->with('status','Anda berhasil mengubah data produk');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::findOrFail($id);

        if (!$product->image == NULL){
            unlink(public_path($product->image));
        }

        Product::destroy($id);
        return redirect()->back()->with('status','Anda berhasil menghapus produk');
    }

    public function publish($id)
    {
        $product = Product::where('id',$id)->first();
        $product->status = 'publish';
        $product->save();

        return redirect()->back()->with('status','Anda berhasil mengubah status produk');
    }

    public function draft($id)
    {
        $product = Product::where('id',$id)->first();
        $product->status = 'draft';
        $product->save();

        return redirect()->back()->with('status','Anda berhasil mengubah status produk');
    }

    public function search($id)
    {
        $product = Product::findOrFail($id);
        return $product;
    }
}
