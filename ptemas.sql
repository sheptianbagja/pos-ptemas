-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 19 Mar 2019 pada 17.33
-- Versi server: 10.1.37-MariaDB
-- Versi PHP: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ptemas`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` enum('publish','draft') COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `categories`
--

INSERT INTO `categories` (`id`, `name`, `slug`, `created_at`, `updated_at`, `status`) VALUES
(1, 'baju pria', 'baju-pria', '2019-03-18 12:55:05', NULL, 'publish'),
(2, 'baju wanita', 'baju-wanita', '2019-03-18 12:55:05', NULL, 'publish'),
(3, 'handphone', 'handphone', '2019-03-18 12:55:05', NULL, 'publish'),
(4, 'laptop', 'laptop', '2019-03-18 12:55:05', NULL, 'publish'),
(5, 'kamera', 'kamera', '2019-03-18 12:55:05', NULL, 'publish'),
(6, 'buku', 'buku', '2019-03-18 12:55:05', NULL, 'publish'),
(7, 'jersey bola', 'software', '2019-03-18 12:55:05', NULL, 'publish'),
(8, 'celana pria', 'celana pria', '2019-03-18 12:55:05', NULL, 'publish'),
(9, 'celana wanita', 'celana wanit', '2019-03-18 12:55:05', NULL, 'publish'),
(10, 'mainan anak', 'mainan-anak', '2019-03-18 12:55:05', NULL, 'publish'),
(11, 'figur', 'figur', '2019-03-18 12:55:05', NULL, 'publish'),
(12, 'gundam', 'gundam', '2019-03-18 12:55:05', NULL, 'publish'),
(13, 'alat kesehatan', 'alat-kesehatan', '2019-03-18 12:55:05', NULL, 'publish'),
(14, 'snack', 'snack', '2019-03-18 12:55:05', NULL, 'publish'),
(15, 'gundam', 'gundam', '2019-03-18 12:55:05', '2019-03-19 06:56:31', 'publish');

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_transactions`
--

CREATE TABLE `detail_transactions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `transaction_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `qty` int(11) NOT NULL,
  `diskon` int(11) NOT NULL,
  `subtotal` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `detail_transactions`
--

INSERT INTO `detail_transactions` (`id`, `transaction_id`, `product_id`, `qty`, `diskon`, `subtotal`, `created_at`, `updated_at`) VALUES
(32, 20, 21, 3, 50, 450000, '2019-03-19 01:55:07', '2019-03-19 01:55:07'),
(33, 21, 5, 1, 20, 136000, '2019-03-19 06:38:12', '2019-03-19 06:38:12'),
(34, 22, 5, 2, 30, 238000, '2019-03-19 06:38:53', '2019-03-19 06:38:53'),
(35, 22, 18, 4, 30, 53200, '2019-03-19 06:38:53', '2019-03-19 06:38:53'),
(36, 22, 19, 10, 5, 180500, '2019-03-19 06:38:53', '2019-03-19 06:38:53'),
(37, 23, 18, 2, 10, 34200, '2019-03-19 06:39:35', '2019-03-19 06:39:35'),
(38, 23, 21, 10, 10, 2700000, '2019-03-19 06:39:35', '2019-03-19 06:39:35'),
(39, 24, 21, 4, 20, 960000, '2019-03-19 06:40:22', '2019-03-19 06:40:22'),
(40, 24, 19, 3, 20, 45600, '2019-03-19 06:40:22', '2019-03-19 06:40:22'),
(41, 25, 21, 2, 10, 540000, '2019-03-19 06:41:03', '2019-03-19 06:41:03'),
(42, 25, 4, 1, 20, 1600000, '2019-03-19 06:41:03', '2019-03-19 06:41:03'),
(43, 26, 14, 2, 10, 9540000, '2019-03-19 06:41:59', '2019-03-19 06:41:59'),
(44, 26, 19, 3, 10, 51300, '2019-03-19 06:41:59', '2019-03-19 06:41:59'),
(45, 27, 2, 2, 5, 22800, '2019-03-19 06:42:42', '2019-03-19 06:42:42'),
(46, 27, 14, 2, 15, 9010000, '2019-03-19 06:42:42', '2019-03-19 06:42:42'),
(47, 28, 12, 4, 3, 58200, '2019-03-19 06:43:17', '2019-03-19 06:43:17'),
(48, 28, 19, 3, 30, 39900, '2019-03-19 06:43:17', '2019-03-19 06:43:17'),
(49, 29, 9, 1, 3, 164900, '2019-03-19 06:43:52', '2019-03-19 06:43:52'),
(50, 29, 11, 2, 5, 323000, '2019-03-19 06:43:52', '2019-03-19 06:43:52'),
(51, 30, 12, 2, 30, 21000, '2019-03-19 06:44:25', '2019-03-19 06:44:25'),
(52, 30, 21, 2, 20, 480000, '2019-03-19 06:44:25', '2019-03-19 06:44:25'),
(53, 31, 19, 30, 20, 456000, '2019-03-19 06:44:43', '2019-03-19 06:44:43'),
(54, 32, 21, 2, 30, 420000, '2019-03-19 06:45:13', '2019-03-19 06:45:13'),
(55, 32, 5, 2, 30, 238000, '2019-03-19 06:45:13', '2019-03-19 06:45:13'),
(56, 32, 18, 3, 30, 39900, '2019-03-19 06:45:13', '2019-03-19 06:45:13'),
(57, 33, 19, 2, 15, 32300, '2019-03-19 06:45:43', '2019-03-19 06:45:43'),
(58, 33, 21, 2, 30, 420000, '2019-03-19 06:45:43', '2019-03-19 06:45:43'),
(59, 34, 17, 1, 30, 9800000, '2019-03-19 06:46:28', '2019-03-19 06:46:28'),
(60, 35, 12, 40, 15, 510000, '2019-03-19 06:46:57', '2019-03-19 06:46:57'),
(61, 36, 4, 3, 20, 4800000, '2019-03-19 06:47:57', '2019-03-19 06:47:57'),
(62, 37, 19, 2, 30, 26600, '2019-03-19 06:48:20', '2019-03-19 06:48:20'),
(63, 38, 12, 40, 20, 480000, '2019-03-19 06:48:43', '2019-03-19 06:48:43'),
(64, 39, 21, 2, 30, 420000, '2019-03-19 06:49:21', '2019-03-19 06:49:21'),
(65, 40, 17, 1, 15, 11900000, '2019-03-19 06:49:40', '2019-03-19 06:49:40');

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_03_16_030230_create_categories_table', 1),
(4, '2019_03_16_072004_add_field_status_to_categories_table', 1),
(5, '2019_03_16_085155_create_products_table', 1),
(6, '2019_03_16_173127_create_stocks_table', 1),
(7, '2019_03_17_023356_add_field_stok_to_table_products', 1),
(8, '2019_03_17_124200_create_transactions_table', 1),
(9, '2019_03_17_130208_create_detail_transactions_table', 1),
(10, '2019_03_18_045036_create_refunds_table', 1),
(11, '2019_03_18_045448_add_field_status_to_transactions_table', 1),
(12, '2019_03_18_052729_add_field_tanggal_to_refunds_table', 1),
(13, '2019_03_18_112120_add_field_nama_customer_to_refunds_table', 1),
(14, '2019_03_18_120325_add_field_total_bayar_and_kembalian_to_transactions_table', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('publish','draft') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `stok` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `products`
--

INSERT INTO `products` (`id`, `category_id`, `name`, `description`, `price`, `image`, `color`, `status`, `created_at`, `updated_at`, `stok`) VALUES
(1, 4, 'Acer Aspire M5-583P-6428', 'Part Number NX.MEFAA.002\r\nProcessor\r\nProcessor type Intel Core i5 - 4200U\r\nProcessor speed 1600.0 MHz\r\nNumber of Cores 2.0\r\nDisplay\r\nDisplay size 15.6 inches\r\nTouchscreen Yes\r\nResolution 1366 x 768 pixels\r\nGraphics\r\nGraphics Type Integrated\r\nPrimary Graphics Chipset Intel HD 4400\r\nStorage\r\nDrive 1 size 500.0 GB\r\nDrive 1 type Hard Disk Drive', 2500000, '/upload/products/acer-aspire-m5-583p-6428.jpg', 'Perak', 'publish', '2019-03-18 12:56:31', '2019-03-18 13:11:18', 500),
(2, 14, 'chitato sapi panggang', 'KOMPOSISI :\r\nKentang, minyak kelapa sawit, bumbu rasa sapi panggang (mengandung penguat rasa mononatrium glutamat, dinatrium inosinat, dinatrium guanilat) dan antioksidan (TBHQ).\r\n\r\nTAKARAN PER KEMASAN :\r\nTakaran per kemasan 4\r\n\r\nTAKARAN PER SERVING :\r\nEnegi 100kkal, energi dari lemak 55kkal. % AKG: lemak total 6g, lemak jenuh 2.5g, kolesterol 0mg, protein 2g, karbohidrat total 10g, serat pangan 1g, gula 0g, natrium 110mg, kalium 270mg, vitamin C 14%, zat besi 2%.\r\n\r\nTAKARAN SAJI :\r\nTakaran nilai saji: 19g\r\n\r\nPLU :\r\n10001094', 12000, '/upload/products/chitato-sapi-panggang.jpg', 'coklat', 'publish', '2019-03-18 12:57:22', '2019-03-19 06:55:15', 494),
(3, 4, 'Dell XPS 15', 'Dell XPS 15 mengusung konsep desain ultra ringkas yang membuat laptop ini bisa lebih kecil dari laptop 15 Inch pada umumnya. Layar InfinityEdge memaksimalkan setiap ruang di bodi laptop untuk ditempatkan layar sebesar 15 Inch. Tebal bezel berkurang hingga 5.7 mm sehingga dapat membuat laptop ini memiliki ukuran yang mendekati laptop 14 Inch. Ketebalan 11-17 mm dan berat mulai dari 1.78 kg dan didukung Storage tipe Solid State Drive (SSD) membuat Dell XPS 15 ini termasuk laptop performa tinggi paling ringan yang ada di pasaran.', 28000000, '/upload/products/dell-xps-15.jpg', 'perak', 'publish', '2019-03-18 12:58:30', '2019-03-19 06:57:17', 525),
(4, 3, 'Huawei Nova 2i', 'Huawei nova 2i berlayar FullView 5.9\" design bezel-less, dibekali RAM 4GB, ROM 64GB serta 4 kamera : 2 kamera selfie 13MP + 2MP dan 2 kamera belakang 16MP + 2MP. Dikenal juga sebagai Mate 10 lite, Maimang 6 atau Honor 9i.', 2000000, '/upload/products/huawei-nova-2i.jpg', 'biru', 'publish', '2019-03-18 12:59:24', '2019-03-19 06:47:57', 492),
(5, 7, 'jersey ac milan', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', 170000, '/upload/products/jersey-ac-milan.png', 'hitam merah', 'publish', '2019-03-18 13:00:18', '2019-03-19 06:45:13', 638),
(6, 7, 'jersey arsenal', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', 170000, '/upload/products/jersey-arsenal.jpg', 'merah', 'publish', '2019-03-18 13:00:52', '2019-03-18 23:40:43', 698),
(7, 7, 'jersey chelsea', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', 170000, '/upload/products/jersey-chelsea.jpg', 'biru muda', 'publish', '2019-03-18 13:02:58', '2019-03-18 23:40:43', 640),
(8, 7, 'jersey intermilan', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', 170000, '/upload/products/jersey-intermilan.png', 'biru hitam', 'publish', '2019-03-18 13:03:18', '2019-03-18 23:38:40', 670),
(9, 7, 'jersey manchester city', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', 170000, '/upload/products/jersey-manchester-city.jpg', 'hitam', 'publish', '2019-03-18 13:03:36', '2019-03-19 06:43:52', 247),
(10, 7, 'jersey manchester united', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', 170000, '/upload/products/jersey-manchester-united.jpg', 'hitam', 'publish', '2019-03-18 13:03:57', '2019-03-18 13:12:27', 850),
(11, 7, 'jersey new castle', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', 170000, '/upload/products/jersey-new-castle.jpg', 'hitam putih', 'publish', '2019-03-18 13:04:16', '2019-03-19 06:43:52', 178),
(12, 14, 'qtela kerupung udang', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', 15000, '/upload/products/qtela-kerupung-udang.png', 'biru muda', 'publish', '2019-03-18 13:04:50', '2019-03-19 06:48:43', 213),
(13, 14, 'lays barbeque fiesta', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', 18000, '/upload/products/lays-barbeque-fiesta.jpg', 'orange', 'publish', '2019-03-18 13:05:14', '2019-03-18 23:35:31', 516),
(14, 4, 'lenovo ideapad 320', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', 5300000, '/upload/products/lenovo-ideapad-320.jpg', 'hitam', 'publish', '2019-03-18 13:06:03', '2019-03-19 06:42:42', 446),
(15, 3, 'oppo a83', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', 3700000, '/upload/products/oppo-a83.jpg', 'coklat muda', 'publish', '2019-03-18 13:06:32', '2019-03-18 22:07:07', 449),
(16, 3, 'oppo f7', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', 2800000, '/upload/products/oppo-f7.jpg', 'merah', 'publish', '2019-03-18 13:07:05', '2019-03-18 22:07:07', 449),
(17, 3, 'oppo find x', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', 14000000, '/upload/products/oppo-find-x.jpg', 'hitam', 'publish', '2019-03-18 13:07:56', '2019-03-19 06:49:40', 428),
(18, 14, 'pitatos iga penyet', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', 19000, '/upload/products/pitatos-iga-penyet.jpg', 'hitam', 'publish', '2019-03-18 13:08:24', '2019-03-19 06:45:13', 440),
(19, 14, 'tango vanilla', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', 19000, '/upload/products/tango-vanilla.jpg', 'biru', 'publish', '2019-03-18 13:08:59', '2019-03-19 06:48:20', 2447),
(20, 3, 'vivo v9', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', 2300000, '/upload/products/vivo-v9.jpg', 'coklat muda', 'publish', '2019-03-18 13:09:36', '2019-03-18 13:13:59', 330),
(21, 1, 'Rei Jaket', 'Lorem ipusm Lorem ipusm Lorem ipusm Lorem ipusm Lorem ipusm Lorem ipusm Lorem ipusm Lorem ipusm Lorem ipusm Lorem ipusm Lorem ipusm', 300000, '/upload/products/rei-jaket.jpg', 'ungu', 'publish', '2019-03-18 23:50:52', '2019-03-19 06:49:21', 123),
(23, 6, 'Parka coklat', 'lorem ipsumm lorem ipsummlorem ipsummlorem ipsumm', 250000, '/upload/products/parka-coklat.png', 'coklat', 'publish', '2019-03-19 07:06:16', '2019-03-19 07:06:16', 0),
(24, 14, 'cuttle fish', 'lorem ipsumm lorem ipsummlorem ipsummlorem ipsummlorem ipsumm', 6000, '/upload/products/cuttle-fish.jpg', 'orange', 'draft', '2019-03-19 07:07:00', '2019-03-19 07:10:48', 0),
(25, 14, 'family cheese', 'lorem ipsummlorem ipsummlorem ipsummlorem ipsummlorem ipsumm', 27000, '/upload/products/family-cheese.jpeg', 'merah', 'draft', '2019-03-19 07:07:28', '2019-03-19 07:10:41', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `refunds`
--

CREATE TABLE `refunds` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `transaction_id` bigint(20) UNSIGNED NOT NULL,
  `note` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `tanggal` date NOT NULL,
  `nama_customer` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `refunds`
--

INSERT INTO `refunds` (`id`, `transaction_id`, `note`, `created_at`, `updated_at`, `tanggal`, `nama_customer`) VALUES
(1, 20, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy', '2019-03-19 06:50:44', '2019-03-19 06:50:44', '2019-03-19', 'sheptian'),
(2, 21, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy', '2019-03-19 06:50:49', '2019-03-19 06:50:49', '2019-03-19', 'rahma aulia'),
(3, 22, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy', '2019-03-19 06:51:10', '2019-03-19 06:51:10', '2019-03-19', 'rangga jaya utama'),
(4, 25, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy', '2019-03-19 06:51:16', '2019-03-19 06:51:16', '2019-03-19', 'intan lestari'),
(5, 28, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy', '2019-03-19 06:51:23', '2019-03-19 06:51:23', '2019-03-19', 'kurniawan aditya'),
(6, 34, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy', '2019-03-19 06:51:43', '2019-03-19 06:51:43', '2019-03-19', 'ahmad herdiansyah'),
(7, 37, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy', '2019-03-19 06:51:48', '2019-03-19 06:51:48', '2019-03-19', 'luna intan lesmana'),
(8, 26, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy', '2019-03-19 06:51:55', '2019-03-19 06:51:55', '2019-03-19', 'rudy setiawan'),
(9, 36, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy', '2019-03-19 06:52:00', '2019-03-19 06:52:00', '2019-03-19', 'rafi ahmad'),
(10, 35, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy', '2019-03-19 06:52:05', '2019-03-19 06:52:05', '2019-03-19', 'opik'),
(11, 33, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy', '2019-03-19 06:52:10', '2019-03-19 06:52:10', '2019-03-19', 'adi ramdani');

-- --------------------------------------------------------

--
-- Struktur dari tabel `stocks`
--

CREATE TABLE `stocks` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `stok` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `stocks`
--

INSERT INTO `stocks` (`id`, `product_id`, `stok`, `created_at`, `updated_at`) VALUES
(1, 1, 500, '2019-03-18 13:11:18', '2019-03-18 13:11:18'),
(2, 2, 500, '2019-03-18 13:11:26', '2019-03-18 13:11:26'),
(3, 3, 500, '2019-03-18 13:11:30', '2019-03-18 13:11:30'),
(4, 4, 500, '2019-03-18 13:11:34', '2019-03-18 13:11:34'),
(5, 5, 700, '2019-03-18 13:11:46', '2019-03-18 13:11:46'),
(6, 6, 700, '2019-03-18 13:11:50', '2019-03-18 13:11:50'),
(7, 7, 700, '2019-03-18 13:11:54', '2019-03-18 13:11:54'),
(8, 8, 700, '2019-03-18 13:11:59', '2019-03-18 13:11:59'),
(9, 9, 250, '2019-03-18 13:12:19', '2019-03-18 13:12:19'),
(10, 10, 850, '2019-03-18 13:12:27', '2019-03-18 13:12:27'),
(11, 11, 180, '2019-03-18 13:12:36', '2019-03-18 13:12:36'),
(12, 13, 550, '2019-03-18 13:12:46', '2019-03-18 13:12:46'),
(13, 14, 450, '2019-03-18 13:12:56', '2019-03-18 13:12:56'),
(14, 15, 450, '2019-03-18 13:13:03', '2019-03-18 13:13:03'),
(15, 16, 450, '2019-03-18 13:13:08', '2019-03-18 13:13:08'),
(16, 17, 450, '2019-03-18 13:13:13', '2019-03-18 13:13:13'),
(17, 18, 450, '2019-03-18 13:13:37', '2019-03-18 13:13:37'),
(18, 12, 330, '2019-03-18 13:13:44', '2019-03-18 13:13:44'),
(19, 19, 2500, '2019-03-18 13:13:52', '2019-03-18 13:13:52'),
(20, 20, 330, '2019-03-18 13:13:59', '2019-03-18 13:13:59'),
(21, 21, 150, '2019-03-18 23:51:42', '2019-03-18 23:51:42');

-- --------------------------------------------------------

--
-- Struktur dari tabel `transactions`
--

CREATE TABLE `transactions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tanggal` date NOT NULL,
  `nama_customer` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `handphone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `metode_pembayaran` enum('cash','transfer') COLLATE utf8mb4_unicode_ci NOT NULL,
  `total` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` enum('done','refund') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'done',
  `total_bayar` int(11) NOT NULL,
  `kembalian` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `transactions`
--

INSERT INTO `transactions` (`id`, `tanggal`, `nama_customer`, `alamat`, `handphone`, `metode_pembayaran`, `total`, `created_at`, `updated_at`, `status`, `total_bayar`, `kembalian`) VALUES
(20, '2019-03-19', 'sheptian', 'jalan pagarsih gg.holili no.14 b rt 002 rw 003 kelurahan jamika kecamatan bojongloa kaler bandung', '087824392239', 'cash', 450000, '2019-03-19 01:55:07', '2019-03-19 06:50:44', 'refund', 450000, 0),
(21, '2019-03-19', 'rahma aulia', 'jalan pagarsih gg.holili no.14 b rt 002 rw 003 kelurahan jamika kecamatan bojongloa kaler bandung', '087824392239', 'cash', 136000, '2019-03-19 06:38:12', '2019-03-19 06:50:49', 'refund', 136000, 0),
(22, '2019-03-19', 'rangga jaya utama', 'jalan pagarsih gg.holili no.14 b rt 002 rw 003 kelurahan jamika kecamatan bojongloa kaler bandung', '087824392239', 'cash', 471700, '2019-03-19 06:38:53', '2019-03-19 06:51:10', 'refund', 500000, 28300),
(23, '2019-03-19', 'yuliani putri utami', 'jalan pagarsih gg.holili no.14 b rt 002 rw 003 kelurahan jamika kecamatan bojongloa kaler bandung', '087824392239', 'cash', 2734200, '2019-03-19 06:39:35', '2019-03-19 06:39:35', 'done', 2734200, 0),
(24, '2019-03-19', 'alvian ahja wijaya', 'jalan pagarsih gg.holili no.14 b rt 002 rw 003 kelurahan jamika kecamatan bojongloa kaler bandung', '087824392239', 'cash', 1005600, '2019-03-19 06:40:22', '2019-03-19 06:40:22', 'done', 1100000, 94400),
(25, '2019-03-19', 'intan lestari', 'jalan pagarsih gg.holili no.14 b rt 002 rw 003 kelurahan jamika kecamatan bojongloa kaler bandung', '087824392239', 'transfer', 2140000, '2019-03-19 06:41:03', '2019-03-19 06:51:17', 'refund', 2140000, 0),
(26, '2019-03-19', 'rudy setiawan', 'jalan pagarsih gg.holili no.14 b rt 002 rw 003 kelurahan jamika kecamatan bojongloa kaler bandung', '087824392239', 'cash', 9591300, '2019-03-19 06:41:59', '2019-03-19 06:51:55', 'refund', 9591300, 0),
(27, '2019-03-19', 'nawan tutu syah lampah', 'jalan pagarsih gg.holili no.14 b rt 002 rw 003 kelurahan jamika kecamatan bojongloa kaler bandung', '087824392239', 'cash', 9032800, '2019-03-19 06:42:42', '2019-03-19 06:42:42', 'done', 9033000, 200),
(28, '2019-03-19', 'kurniawan aditya', 'jalan pagarsih gg.holili no.14 b rt 002 rw 003 kelurahan jamika kecamatan bojongloa kaler bandung', '087824392239', 'cash', 98100, '2019-03-19 06:43:17', '2019-03-19 06:51:23', 'refund', 58200, -39900),
(29, '2019-03-19', 'difa faza', 'jalan pagarsih gg.holili no.14 b rt 002 rw 003 kelurahan jamika kecamatan bojongloa kaler bandung', '087824392239', 'cash', 487900, '2019-03-19 06:43:52', '2019-03-19 06:43:52', 'done', 487900, 0),
(30, '2019-03-19', 'bambang', 'jalan pagarsih gg.holili no.14 b rt 002 rw 003 kelurahan jamika kecamatan bojongloa kaler bandung', '087824392239', 'transfer', 501000, '2019-03-19 06:44:25', '2019-03-19 06:44:25', 'done', 501000, 0),
(31, '2019-03-19', 'ilham setiawan', 'jalan pagarsih gg.holili no.14 b rt 002 rw 003 kelurahan jamika kecamatan bojongloa kaler bandung', '087824392239', 'cash', 456000, '2019-03-19 06:44:43', '2019-03-19 06:44:43', 'done', 456000, 0),
(32, '2019-03-19', 'lendra', 'jalan pagarsih gg.holili no.14 b rt 002 rw 003 kelurahan jamika kecamatan bojongloa kaler bandung', '087824392239', 'transfer', 697900, '2019-03-19 06:45:13', '2019-03-19 06:45:13', 'done', 697900, 0),
(33, '2019-03-19', 'adi ramdani', 'jalan pagarsih gg.holili no.14 b rt 002 rw 003 kelurahan jamika kecamatan bojongloa kaler bandung', '087824392239', 'cash', 452300, '2019-03-19 06:45:43', '2019-03-19 06:52:10', 'refund', 452300, 0),
(34, '2019-03-19', 'ahmad herdiansyah', 'jalan pagarsih gg.holili no.14 b rt 002 rw 003 kelurahan jamika kecamatan bojongloa kaler bandung', '087824392239', 'transfer', 9800000, '2019-03-19 06:46:28', '2019-03-19 06:51:43', 'refund', 9800000, 0),
(35, '2019-03-19', 'opik', 'jalan pagarsih gg.holili no.14 b rt 002 rw 003 kelurahan jamika kecamatan bojongloa kaler bandung', '087824392239', 'cash', 510000, '2019-03-19 06:46:57', '2019-03-19 06:52:06', 'refund', 510000, 0),
(36, '2019-03-19', 'rafi ahmad', 'jalan pagarsih gg.holili no.14 b rt 002 rw 003 kelurahan jamika kecamatan bojongloa kaler bandung', '087824392239', 'cash', 4800000, '2019-03-19 06:47:57', '2019-03-19 06:52:00', 'refund', 4800000, 0),
(37, '2019-03-19', 'luna intan lesmana', 'jalan pagarsih gg.holili no.14 b rt 002 rw 003 kelurahan jamika kecamatan bojongloa kaler bandung', '087824392239', 'cash', 26600, '2019-03-19 06:48:20', '2019-03-19 06:51:48', 'refund', 26600, 0),
(38, '2019-03-19', 'henhen sutisna', 'jalan pagarsih gg.holili no.14 b rt 002 rw 003 kelurahan jamika kecamatan bojongloa kaler bandung', '087824392239', 'cash', 480000, '2019-03-19 06:48:43', '2019-03-19 06:48:43', 'done', 480000, 0),
(39, '2019-03-19', 'rido', 'jalan pagarsih gg.holili no.14 b rt 002 rw 003 kelurahan jamika kecamatan bojongloa kaler bandung', '087824392239', 'cash', 420000, '2019-03-19 06:49:21', '2019-03-19 06:49:21', 'done', 420000, 0),
(40, '2019-03-19', 'anton', 'jalan pagarsih gg.holili no.14 b rt 002 rw 003 kelurahan jamika kecamatan bojongloa kaler bandung', '087824392239', 'cash', 11900000, '2019-03-19 06:49:40', '2019-03-19 06:49:40', 'done', 11900000, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@gmail.com', NULL, '$2y$10$Hdgd12JI75.bEUdLmy.k2uirVq3WA1DItAkiPawlhcQYTEwuFdb9.', NULL, '2019-03-18 12:54:48', '2019-03-18 12:54:48');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `detail_transactions`
--
ALTER TABLE `detail_transactions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `detail_transactions_product_id_foreign` (`product_id`),
  ADD KEY `detail_transactions_transaction_id_foreign` (`transaction_id`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indeks untuk tabel `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_category_id_foreign` (`category_id`);

--
-- Indeks untuk tabel `refunds`
--
ALTER TABLE `refunds`
  ADD PRIMARY KEY (`id`),
  ADD KEY `refunds_transaction_id_foreign` (`transaction_id`);

--
-- Indeks untuk tabel `stocks`
--
ALTER TABLE `stocks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `stocks_product_id_foreign` (`product_id`);

--
-- Indeks untuk tabel `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT untuk tabel `detail_transactions`
--
ALTER TABLE `detail_transactions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT untuk tabel `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT untuk tabel `refunds`
--
ALTER TABLE `refunds`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `stocks`
--
ALTER TABLE `stocks`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT untuk tabel `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `detail_transactions`
--
ALTER TABLE `detail_transactions`
  ADD CONSTRAINT `detail_transactions_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `detail_transactions_transaction_id_foreign` FOREIGN KEY (`transaction_id`) REFERENCES `transactions` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `refunds`
--
ALTER TABLE `refunds`
  ADD CONSTRAINT `refunds_transaction_id_foreign` FOREIGN KEY (`transaction_id`) REFERENCES `transactions` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `stocks`
--
ALTER TABLE `stocks`
  ADD CONSTRAINT `stocks_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
